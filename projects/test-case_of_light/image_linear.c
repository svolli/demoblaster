
#include <math.h>
#include <stdlib.h>

#include "global.h"


void image_linear( opc_sink s, alsaplay_t alsa, int end, void *png_data, int png_size )
{
   gdImagePtr im = 0, scaled = 0;
   float pos = 0.0, step = 0.0;
   int xsize = 0, ysize = 0;
   int xmove = 0, ymove = 0;
   int xoffset = 0, yoffset = 0;
   int direction = 0;
   wait_until_t w = wait_until_init();

   im = gdImageCreateFromPngPtr( png_size, png_data );
   if( !im )
   {
      FAIL();
   }

   scaled = gdImageCreateTrueColor( 32, 16 );
   if( (scaled->sx * im->sy) == (im->sx * scaled->sy) )
   {
      /* special case: no movement needed */

      opc_put_gd_image_32x16z( s, im );
      return;
   }
   if( (scaled->sx * im->sy) < (im->sx * scaled->sy) )
   {
      xsize = im->sy * scaled->sx / scaled->sy;
      ysize = im->sy;
      xmove = im->sx - xsize;
      ymove = 0;
      step  = 4.0 * scaled->sy / scaled->sx / im->sy;
   }
   else
   {
      xsize = im->sx;
      ysize = im->sx * scaled->sy / scaled->sx;
      xmove = 0;
      ymove = im->sy - ysize;
      step  = 4.0 * scaled->sx / scaled->sy / im->sx;
   }

   direction = 1;
   gdImageCopyResampled( scaled, im,
                         0, 0, 0, 0,
                         32, 16, xsize, ysize );
   fade_in( s, scaled, 25, 40 );
   wait_until_ms( w, 40 );
   while( alsaplay_pos_time( alsa ) < end )
   {
      pos += direction * step;
      if( pos >= 2.0 )
      {
         direction = 0;
         continue;
      }
      if( pos <= 0.0 )
      {
         direction = 1;
         continue;
      }

      xoffset = pos * xmove / 2;
      yoffset = pos * ymove / 2;
      gdImageCopyResampled( scaled, im,
                            0, 0, xoffset, yoffset,
                            32, 16, xsize, ysize );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, scaled );
   }
   fade_out( s, scaled, 25, 40 );

   gdImageDestroy( im );
   gdImageDestroy( scaled );
   wait_until_done( w );
}
