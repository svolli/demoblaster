
#include <math.h>
#include <stdlib.h>

#include "global.h"


void image_sinus( opc_sink s, alsaplay_t alsa, int end, void *png_data, int png_size )
{
   gdImagePtr im, scaled;
   float sinpos = 0.0;
   int xsize, ysize, xmove, ymove, xoffset, yoffset;
   wait_until_t w = wait_until_init();

   im = gdImageCreateFromPngPtr( png_size, png_data );
   if( !im )
   {
      FAIL();
   }
   scaled = gdImageCreateTrueColor( 32, 16 );
   if( (scaled->sx * im->sy) == (im->sx * scaled->sy) )
   {
      /* special case: no movement needed */

      opc_put_gd_image_32x16z( s, im );
      return;
   }
   if( (scaled->sx * im->sy) < (im->sx * scaled->sy) )
   {
      xsize = im->sy * scaled->sx / scaled->sy;
      ysize = im->sy;
      xmove = im->sx - xsize;
      ymove = 0;
   }
   else
   {
      xsize = im->sx;
      ysize = im->sx * scaled->sy / scaled->sx;
      xmove = 0;
      ymove = im->sy - ysize;
   }

   wait_until_ms( w, 40 );
   while( alsaplay_pos_time( alsa ) < end )
   {
      sinpos += 0.033;
      if( sinpos >= M_PI * 2 )
      {
         sinpos -= M_PI * 2;
      }

      xoffset = (sinf(sinpos)+1) * xmove / 2;
      yoffset = (sinf(sinpos)+1) * ymove / 2;
      gdImageCopyResampled( scaled, im,
                            0, 0, xoffset, yoffset,
                            32, 16, xsize, ysize );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, scaled );
   }

   gdImageDestroy( im );
   gdImageDestroy( scaled );
   wait_until_done( w );
}
