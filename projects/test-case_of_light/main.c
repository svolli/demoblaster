
#include <stdio.h>
#include <stdlib.h>
#include "global.h"

#include "media/credits.h"
#include "media/trsi_scene_drink.h"
#include "media/super_world.h"

#define INLINE_AUDIO 1
#if INLINE_AUDIO
//#define OGG_SOUNDTRACK_SIZE (10846707)
//extern const unsigned char OGG_SOUNDTRACK[10846707];
extern const unsigned char _binary_media_tEiS___the_suit_case_ogg_end;
extern const unsigned char _binary_media_tEiS___the_suit_case_ogg_start;
#define OGG_SOUNDTRACK_SIZE (&_binary_media_tEiS___the_suit_case_ogg_end-&_binary_media_tEiS___the_suit_case_ogg_start)
#define OGG_SOUNDTRACK (&_binary_media_tEiS___the_suit_case_ogg_start)
#endif


/*
- TRSI (zoomed out logo from end fades in?)
- present
- Test-Case of Light
- MarioLand Scroll
- Ellipses
- Into The Blue
- Twister
- Equalizer Scene Drink
- Minion Credits
- Greetings
  -> zoom out to TRSI
 */

#if 0
void log_time( const char *name, int ms )
{
   printf( "%10s: %6d (%d:%02d)\n", name, ms, ms / 60000, (ms/1000) % 60 );
}
#else
#define log_time( a, b ) /**/
#endif

int main( int argc, char *argv[] )
{
   alsaplay_t alsa;
#if INLINE_AUDIO
#else
   int err;
#endif
   OggVorbis_File vf;
   opc_sink s;
   image_zoom_t trsi_greetings;

   s = opc_new_sink( 0 );
   alsa = alsaplay_init( &vf );
   trsi_greetings = trsi_zoom_init();

#if INLINE_AUDIO
#if 0
   printf( "start:%p\nend  :%p\nsize :%ld\nsize :%ld (10846707)\n", 
           &_binary_media_tEiS___the_suit_case_ogg_start,
           &_binary_media_tEiS___the_suit_case_ogg_end,
           *(&_binary_media_tEiS___the_suit_case_ogg_size),
           OGG_SOUNDTRACK_SIZE );
#endif
   ov_load_data( (const char*)OGG_SOUNDTRACK, OGG_SOUNDTRACK_SIZE, &vf );
   alsaplay_setup( alsa, &vf );
#else
   if( argc < 2 )
   {
      fprintf( stderr, "usage: %s <name of .ogg> ...\n", argv[0] );
      return 0;
   }
   err = alsaplay_file( alsa, argv[1] );
   if( err )
   {
      perror( "alsaplay_file" );
      exit( EXIT_FAILURE );
   }
#endif

   /* step 3: start playing thread */
   //alsaplay_start( alsa );

   /* TRSI */

   log_time( "title", alsaplay_pos_time( alsa ) );
   trsi_title( s, trsi_greetings, alsa, 5000 );
#if 1
   log_time( "present", alsaplay_pos_time( alsa ) );
   present_draw( s );

   log_time( "logo", alsaplay_pos_time( alsa ) );
   logo_draw( s );

   log_time( "world", alsaplay_pos_time( alsa ) );
   image_linear( s, alsa, 28500,
                 (void*)PNG_SUPER_WORLD, PNG_SUPER_WORLD_SIZE );

   log_time( "ellies", alsaplay_pos_time( alsa ) );
   ellipses( s, alsa, 59000 );

   log_time( "blue", alsaplay_pos_time( alsa ) );
   into_the_blue( s, alsa, 88500 );

   log_time( "twister", alsaplay_pos_time( alsa ) );
   twister( s, alsa, 133000 );

   log_time( "can", alsaplay_pos_time( alsa ) );
   image_sinus_analyzer( s, alsa, 154000,
                         (void*)PNG_TRSI_SCENE_DRINK, PNG_TRSI_SCENE_DRINK_SIZE );

   log_time( "spotter", alsaplay_pos_time( alsa ) );
   credits( s, (void*)PNG_CREDITS_GFX_LEAD,  PNG_CREDITS_GFX_LEAD_SIZE,
               (void*)PNG_CREDITS_TXT_LEAD,  PNG_CREDITS_TXT_LEAD_SIZE );
   log_time( "thanks", alsaplay_pos_time( alsa ) );
   thank_slide( s );
   log_time( "prince", alsaplay_pos_time( alsa ) );
   credits( s, (void*)PNG_CREDITS_GFX_GFX,   PNG_CREDITS_GFX_GFX_SIZE,
               (void*)PNG_CREDITS_TXT_GFX,   PNG_CREDITS_TXT_GFX_SIZE );
   log_time( "teis", alsaplay_pos_time( alsa ) );
   credits( s, (void*)PNG_CREDITS_GFX_MUSIC, PNG_CREDITS_GFX_MUSIC_SIZE,
               (void*)PNG_CREDITS_TXT_MUSIC, PNG_CREDITS_TXT_MUSIC_SIZE );
   log_time( "svolli", alsaplay_pos_time( alsa ) );
   credits( s, (void*)PNG_CREDITS_GFX_CODE,  PNG_CREDITS_GFX_CODE_SIZE,
               (void*)PNG_CREDITS_TXT_CODE,  PNG_CREDITS_TXT_CODE_SIZE );

#else
   ov_time_seek( &vf, 196750 );
#endif
   log_time( "greets", alsaplay_pos_time( alsa ) );
   greetings( s, trsi_greetings, alsa, 409000 );
   log_time( "end",    alsaplay_pos_time( alsa ) );

   trsi_zoom_done( trsi_greetings );
   /* step 4: stop playing thread */
   alsaplay_stop( alsa );
   alsaplay_done( alsa );

   return 0;
}

