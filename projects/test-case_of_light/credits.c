
#include <math.h>
#include <stdlib.h>

#include "global.h"


struct copperbars_s
{
   float frame[2];
   float line[2];
};

typedef struct copperbars_s *copperbars_t;


copperbars_t copperbars_init()
{
   int i;
   copperbars_t d = (copperbars_t)malloc( sizeof(struct copperbars_s) );
   for( i = 0; i < 2; ++i )
   {
      d->frame[i] = 0.0;
      d->line[i] = 0.0;
   }
   return d;
}


void copperbars_done( copperbars_t d )
{
   free( d );
}

void copperbars_frame( copperbars_t d )
{
   int i;
   for( i = 0; i < 2; ++i )
   {
      d->frame[i] += 10.0 / 360;
      while( d->frame[i] >= 1.0 )
      {
         d->frame[i] -= 1.0;
      }
      d->line[i] = d->frame[i];
   }
}


void copperbars_line( copperbars_t d, hsv_t *color )
{
   d->line[0] += 10.0 / 360;
   d->line[1] -= 30.0 / 360;
   while( d->line[0] >= 1.0 )
   {
      d->line[0] -= 1.0;
   }
   while( d->line[1] < 0.0 )
   {
      d->line[1] += 1.0;
   }

   color->h = ( sinf(d->line[1]) +
               (sinf(d->line[0]) / 2) );

}


void thank_slide( opc_sink s )
{
   int i = 0;
   wait_until_t w = wait_until_init();

   gdImagePtr text = gdImageCreateTrueColor( 32, 10*8 );
   gdImagePtr im =  gdImageCreateTrueColor( 32, 16 );
   gdImageString( text, gdFontGet4x8(),  6, 2*8, (unsigned char*)"WOULD", 0xf5deb3 );
   gdImageString( text, gdFontGet4x8(),  8, 3*8, (unsigned char*)"LIKE", 0xf5deb3 );
   gdImageString( text, gdFontGet4x8(), 12, 4*8, (unsigned char*)"TO", 0xf5deb3 );
   gdImageString( text, gdFontGet4x8(),  6, 5*8, (unsigned char*)"THANK", 0xf5deb3 );
   gdImageString( text, gdFontGet4x8(), 10, 6*8, (unsigned char*)"HIS", 0xf5deb3 );
   gdImageString( text, gdFontGet4x8(),  2, 7*8, (unsigned char*)"MINIONS", 0xf5deb3 );

   wait_until_ms( w, 40 );
   for( i = 0; i < text->sy - im->sy; ++i )
   {
      gdImageCopy( im, text, 0, 0, 0, i, im->sx, im->sy );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );
   }
}


void credits( opc_sink s,
              void *gfx_data, int gfx_size,
              void *txt_data, int txt_size )
{
   gdImagePtr im = 0, scaled = 0;
   gdImagePtr text = 0, merged = 0;
   static float sinpos = M_PI_2 + M_PI;
   copperbars_t c = copperbars_init();
   hsv_t coppercolor;
   int xsize = 0, ysize = 0;
   int xmove = 0, ymove = 0;
   int xoffset = 0, yoffset = 0;
   int pct = 0, color = 0;
   int x = 0, y = 0;
   wait_until_t w = wait_until_init();

   coppercolor.s = 1.0;
   coppercolor.v = 1.0;
   im = gdImageCreateFromPngPtr( gfx_size, gfx_data );
   if( !im )
   {
      FAIL();
   }
   text = gdImageCreateFromPngPtr( txt_size, txt_data );
   if( !text )
   {
      FAIL();
   }
   scaled = gdImageCreateTrueColor( 32, 16 );
   merged = gdImageCreateTrueColor( scaled->sx, scaled->sy );
   gdImageColorTransparent( merged, gdImageColorExact( merged, 0, 0, 0xFF ) );
   if( (scaled->sx * im->sy) == (im->sx * scaled->sy) )
   {
      /* special case: no movement needed */

      opc_put_gd_image_32x16z( s, im );
      return;
   }
   if( (scaled->sx * im->sy) < (im->sx * scaled->sy) )
   {
      xsize = im->sy * scaled->sx / scaled->sy;
      ysize = im->sy;
      xmove = im->sx - xsize;
      ymove = 0;
   }
   else
   {
      xsize = im->sx;
      ysize = im->sx * scaled->sy / scaled->sx;
      xmove = 0;
      ymove = im->sy - ysize;
   }

   wait_until_ms( w, 40 );
   
   for( pct = 0; pct < 100; pct += 4 )
   {
      sinpos += 0.07;
      if( sinpos >= M_PI * 2 )
      {
         sinpos -= M_PI * 2;
      }

      xoffset = (sinf(sinpos)+1) * xmove / 2;
      yoffset = (sinf(sinpos)+1) * ymove / 2;
      
      gdImageCopyResampled( scaled, im,
                            0, 0, xoffset, yoffset,
                            32, 16, xsize, ysize );
      gdImageFilledRectangle( merged, 0, 0, merged->sx, merged->sy, 0x000000 );
      gdImageCopyMerge( merged, scaled, 0, 0, 0, 0, scaled->sx, scaled->sy, pct );

      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, merged );
   }
   
   for( pct = 0; pct < 200; ++pct )
   {
      sinpos += 0.07;
      if( sinpos >= M_PI * 2 )
      {
         sinpos -= M_PI * 2;
      }

      xoffset = (sinf(sinpos)+1) * xmove / 2;
      yoffset = (sinf(sinpos)+1) * ymove / 2;
      gdImageCopyResampled( scaled, im,
                            0, 0, xoffset, yoffset,
                            32, 16, xsize, ysize );
      copperbars_frame( c );
      for( y = 0; y < merged->sy; ++y )
      {
         copperbars_line( c, &coppercolor );
         for( x = 0; x < merged->sx; ++x )
         {
            if( gdImageGetTransparent( text ) )
            {
               color = gdImageGetTrueColorPixel( text, x, y );
            }
            else
            {
               int index = gdImageGetPixel( text, x, y );
               color = gdImageRed( text, index )   << 16 |
                       gdImageGreen( text, index ) << 8 |
                       gdImageBlue( text, index );
            }
            if( color == 0x000000 )
            {
               color = hsv_to_rgb( &coppercolor );
            }
            else
            {
               color = gdImageGetPixel( scaled, x, y );
               color &= 0xFCFCFC;
               color >>= 2;
            }
            gdImageSetPixel( merged, x, y, color );
         }
      }

      if( pct < 100 )
      {
         gdImageCopyMerge( merged, scaled, 0, 0, 0, 0, scaled->sx, scaled->sy, 100-pct );
      }
      else
      {
         gdImageCopyMerge( merged, scaled, 0, 0, 0, 0, scaled->sx, scaled->sy, pct-100 );
      }
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, merged );
   }

   for( pct = 0; pct < 100; pct += 4 )
   {
      sinpos += 0.07;
      if( sinpos >= M_PI * 2 )
      {
         sinpos -= M_PI * 2;
      }

      xoffset = (sinf(sinpos)+1) * xmove / 2;
      yoffset = (sinf(sinpos)+1) * ymove / 2;
      
      gdImageCopyResampled( scaled, im,
                            0, 0, xoffset, yoffset,
                            32, 16, xsize, ysize );
      gdImageFilledRectangle( merged, 0, 0, merged->sx, merged->sy, 0x000000 );
      gdImageCopyMerge( merged, scaled, 0, 0, 0, 0, scaled->sx, scaled->sy, 100-pct );

      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, merged );
   }

   gdImageDestroy( im );
   gdImageDestroy( scaled );
   gdImageDestroy( text );
   gdImageDestroy( merged );
   wait_until_done( w );
   copperbars_done( c );
}
