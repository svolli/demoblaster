
#include <math.h>
#include <stdlib.h>

#include "global.h"

#include "media/trsi_greetings.h"

#if 0
static image_zoom_t z;
static gdImagePtr zim;
#endif

/*
 *   15,  20 ->  570,  20 : Greetings
 *   78,  80 ->  493,  80 : The Black Lotus
 *  250, 155 ->  250, 495 : Booze Design
 *  215, 565 ->  265, 565 : LFT
 *  250, 990 ->  250, 660 : Panda Design
 *  320, 880 ->  320, 990 : Titan
 *  320, 780 ->  320, 155 : DDK
 *
 *  660,  60 ->  660, 990 : ASD
 *  720, 980 ->  720, 620 : Brain Control
 *  720, 405 ->  720, 135 : Conspiracy
 *  700,  20 ->  975,  20 : Rabenauge
 *  740,  80 ->  920,  80 : Arsenic
 * 1025,  45 -> 1125, 210 : Mercury
 * 1025, 430 -> 1115, 275 : Nuance
 *  760, 470 ->  970, 470 : Code Red
 *  720, 525 ->  895, 525 : Kaomau
 *  945, 560 -> 1100, 990 : Abyss Connection
 *  930, 740 -> 1024, 990 : Dekadence
 *
 * 1325, 255 -> 1455,  20 -> 1750, 20 : AA
 * 1490,  80 -> 1755,  80 : Onslaught
 * 1405, 235 -> 1710, 410 : Gaspode
 * 1355, 295 -> 1440, 440 : Oxyron
 * 1480, 470 -> 1630, 470 : Hitmen
 * 1495, 525 -> 1620, 525 : SVatG
 * 1675, 500 -> 1770, 665 : Haujobb
 * 1650, 595 -> 1650, 850 : Ghostown
 * 1695, 920 -> 1760, 735 : rtificial
 * 1320, 920 -> 1605, 920 : Farbrausch
 * 1305, 990 -> 1705, 990 : G*P
 *
 * 1915,  20 -> 2015,  20 : TRBL
 * 1915,  65 -> 2025,  65 : MEGA
 * 1940, 495 -> 1940, 205 : Dreams
 * 2010, 205 -> 2010, 755 : DSS
 * 2010, 995 -> 2010, 815 : Cluster
 * 1940, 610 -> 1940, 995 : VCS
 */

typedef enum { LINEAR, JUMP, JUMP_IN, JUMP_OUT } move_type_t;

typedef struct {
   move_type_t type;
   int         sx;
   int         sy;
   float       sh;
   int         ex;
   int         ey;
   float       eh;
} move_to_t;

static const int start = 0;
static const float jump_height = 16.0;
move_to_t move_list[] = {
   /* type,  sx, sy, sh, ex, ey, eh */
   { JUMP,   1324, 712, 1.0,   15,  20, 2.5 },
   { LINEAR,   15,  20, 2.5,  570,  20, 2.5 }, // Greetings
   { JUMP,    570,  20, 2.5,  945, 560, 1.8 },
   { LINEAR,  945, 560, 1.8, 1110,1000, 1.8 }, // Abyss Connection
   { JUMP,   1110,1000, 1.8, 1325, 255, 2.5 },
   { LINEAR, 1325, 255, 2.5, 1455,  20, 2.5 }, // Aa 1/2
   { LINEAR, 1455,  20, 2.5, 1750,  20, 2.5 }, // aA 2/2
   { JUMP,   1750,  20, 2.5,  659,  40, 1.3 },
   { LINEAR,  659,  40, 1.3,  659,1006, 1.3 }, // ASD
   { JUMP,    659,1006, 1.3,  740,  80, 2.5 },
   { LINEAR,  740,  80, 2.5,  917,  80, 2.5 }, // Arsenic

   { JUMP,    917,  80, 2.5,   78,  80, 2.5 },
   { LINEAR,   78,  80, 2.5,  493,  80, 2.5 }, // Black Lotus
   { JUMP,    493,  80, 2.5,  248, 140, 1.3 },
   { LINEAR,  248, 140, 1.3,  248, 495, 1.3 }, // Booze Design
   { JUMP,    248, 495, 1.3,  719, 990, 1.3 },
   { LINEAR,  719, 990, 1.3,  719, 600, 1.3 }, // Brain Control
   { JUMP,    719, 600, 1.3, 2010,1010, 1.3 },
   { LINEAR, 2010,1010, 1.3, 2010, 807, 1.3 }, // Cluster
   { JUMP,   2010, 807, 1.3,  760, 470, 2.5 },
   { LINEAR,  760, 470, 2.5,  970, 470, 2.5 }, // Code Red
   { JUMP,    970, 470, 2.5,  719, 420, 1.3 },
   { LINEAR,  719, 420, 1.3,  719, 125, 1.3 }, // Conspiracy

   { JUMP,    719, 125, 1.3,  920, 720, 1.8 },
   { LINEAR,  920, 720, 1.8, 1026,1000, 1.8 }, // Dekadence
   { JUMP,   1026,1000, 1.8,  318, 800, 1.3 },
   { LINEAR,  318, 800, 1.3,  318, 140, 1.3 }, // DDK
   { JUMP,    318, 140, 1.3, 2010, 195, 1.3 },
   { LINEAR, 2010, 195, 1.3, 2010, 765, 1.3 }, // DSS
   { JUMP,   2010, 765, 1.3, 1940, 505, 1.3 },
   { LINEAR, 1940, 505, 1.3, 1940, 195, 1.3 }, // Dreams
   { JUMP,   1940, 195, 1.3, 1320, 920, 2.5 },
   { LINEAR, 1320, 920, 2.5, 1605, 920, 2.5 }, // Farbrausch

   { JUMP,   1605, 920, 2.5, 1015,  35, 1.8 },
   { LINEAR, 1015,  35, 1.8, 1125, 230, 1.8 }, // Gaspode
   { JUMP,   1125, 230, 1.8, 1305, 994, 3.2 },
   { LINEAR, 1305, 994, 3.2, 1705, 994, 3.2 }, // G*P !
   { JUMP,   1705, 994, 3.2, 1650, 590, 1.3 },
   { LINEAR, 1650, 590, 1.3, 1650, 860, 1.3 }, // Ghostown !
   { JUMP,   1650, 860, 1.3, 1673, 490, 2.0 },
   { LINEAR, 1673, 490, 2.0, 1765, 670, 2.0 }, // Haujobb
   { JUMP,   1765, 670, 2.0, 1480, 470, 2.5 },
   { LINEAR, 1480, 470, 2.5, 1630, 470, 2.5 }, // Hitmen

   { JUMP,   1630, 470, 2.5,  720, 530, 2.5 },
   { LINEAR,  720, 530, 2.5,  895, 530, 2.5 }, // Kaomau
   { JUMP,    895, 530, 2.5,  210, 569, 2.5 },
   { LINEAR,  210, 569, 2.5,  260, 569, 2.5 }, // LFT
   { LINEAR,  260, 569, 2.5,  237, 569, 2.5 }, // LFT
   { JUMP,    237, 569, 2.5, 1915,  70, 2.5 },
   { LINEAR, 1915,  70, 2.5, 2025,  70, 2.5 }, // MEGA
   { JUMP,   2025,  70, 2.5, 1399, 220, 1.8 },
   { LINEAR, 1399, 220, 1.8, 1510, 405, 1.8 }, // Mercury
   { JUMP,   1510, 405, 1.8, 1000, 355, 1.8 },
   { LINEAR, 1000, 355, 1.8, 1062, 245, 1.8 }, // Miss
   { LINEAR, 1062, 245, 1.8, 1000, 135, 1.8 }, // Ctrl
   { JUMP,   1000, 135, 1.8, 1015, 440, 1.8 },
   { LINEAR, 1015, 440, 1.8, 1115, 275, 1.8 }, // Nuance

   { JUMP,   1115, 275, 1.8, 1490,  80, 2.5 },
   { LINEAR, 1490,  80, 2.5, 1755,  80, 2.5 }, // Onslaught
   { JUMP,   1755,  80, 2.5, 1355, 290, 1.8 },
   { LINEAR, 1355, 290, 1.8, 1434, 444, 1.8 }, // Oxyron
   { JUMP,   1434, 444, 1.8,  250,1000, 1.3 },
   { LINEAR,  250,1000, 1.3,  250, 640, 1.3 }, // Panda Design
   { JUMP,    250, 640, 1.3,  700,  20, 2.5 },
   { LINEAR,  700,  20, 2.5,  965,  20, 2.5 }, // Rabenauge
   { JUMP,    965,  20, 2.5, 1690, 930, 1.8 },
   { LINEAR, 1690, 930, 1.8, 1750, 745, 1.8 }, // rtificial
   { JUMP,   1750, 745, 1.8, 1495, 528, 2.5 },
   { LINEAR, 1495, 528, 2.5, 1620, 528, 2.5 }, // SVatG

   { JUMP,   1620, 528, 2.5,  316, 880, 1.3 },
   { LINEAR,  316, 880, 1.3,  316,1000, 1.3 }, // Titan
   { JUMP,    316,1000, 1.3, 1915,  20, 2.5 },
   { LINEAR, 1915,  20, 2.5, 2015,  20, 2.5 }, // TRBL
   { JUMP,   2015,  20, 2.5, 1938, 600, 1.3 },
   { LINEAR, 1938, 600, 1.3, 1938,1010, 1.3 }, // VCS

   {JUMP_OUT,1938,1010, 1.3, 1024, 512,64.0 },
};

struct image_zoom_s
{
   gdImagePtr  im;
   int         listsize;
   gdImagePtr  *list;
   int         *scale;
};

void _image_zoom_calc( image_zoom_t d, int cx, int cy, float zoom )
{
   int idx = 0;
   int i = 1;
   int sizex  = (int)(zoom * d->im->sx);
   int sizey  = (int)(zoom * d->im->sy);
   int startx = cx - sizex/2;
   int starty = cy - sizey/2;
printf( "i:(%4d,%4d),%f,d->im(%d,%d),%p,%p\n", cx, cy, zoom, d->im->sx, d->im->sy,d,d->im );
printf( "u:(%4d,%4d)->(%4d,%4d)\n", startx, starty, sizex, sizey );

   for( i = 0; i < d->listsize; ++i )
   {
      if( zoom >= d->scale[i] * d->scale[i] / 2 )
      {
         idx = i;
      }
      else
      {
         break;
      }
   }
   sizex  /= d->scale[idx];
   sizey  /= d->scale[idx];
   startx /= d->scale[idx];
   starty /= d->scale[idx];

printf( "s:(%4d,%4d)->(%4d,%4d)\n", startx, starty, sizex, sizey );
   //gdImageCopyResampled( d->im, d->list[idx], 0, 0, startx, starty, d->im->sx, d->im->sy, sizex, sizey );
   gdImageCopyResized( d->im, d->list[idx], 0, 0, startx, starty, d->im->sx, d->im->sy, sizex, sizey );
}

static gdImagePtr zim;

image_zoom_t trsi_zoom_init()
{
   gdImagePtr src;
   image_zoom_t z;
   
   src = gdImageCreateFromPngPtr( PNG_TRSI_GREETINGS_SIZE, (void*)PNG_TRSI_GREETINGS );
   zim = gdImageCreateTrueColor( 32, 16 );
   z = image_zoom_init( zim, src );
   gdImageDestroy( src );
   
   return z;
}

void trsi_zoom_done( image_zoom_t z )
{
   image_zoom_done( z );
   gdImageDestroy( zim );
}

void trsi_title( opc_sink s, image_zoom_t z, alsaplay_t alsa, int end )
{
   wait_until_t w = wait_until_init();

   image_zoom_calc( z, 1024, 512, 64.0 );

   image_normalize( zim );
   fade_in( s, zim, 25, 40 );
   alsaplay_start( alsa );
   wait_until_ms( w, 40 );
   while( alsaplay_pos_time( alsa ) < end )
   {
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
   wait_until_done( w );
}


void present_draw( opc_sink s )
{
   int i, x, y;
   wait_until_t w = wait_until_init();
   gdImagePtr im = gdImageCreateTrueColor( 32, 16 );
   gdImagePtr present = gdImageCreateTrueColor( 28, 8 );
   gdImageString( present, gdFontGet4x8(), 0, 0, (unsigned char*)"PRESENT", 0xf5deb3 );
   gdImageColorTransparent( im, 0 );
   gdImageColorTransparent( present, 0 );

   wait_until_ms( w, 40 );
   for( i = 0; i < 60; ++i )
   {
      x = 32-i;
      if( i < 30 )
      {
         y = cosf( M_PI * i / 60) * 8;
      }
      else
      {
         y = cosf( M_PI * (60-i) / 60) * 8;
      }
      //gdImageFilledRectangle( im, 0, 0, im->sx-1, im->sy-1, 0x000000 );
      gdImageCopy( im, zim, 0, 0, 0, 0, im->sx, im->sy );
      gdImageCopy( im, present, x, 8-y, 0, 0, present->sx, present->sy );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );
   }

   gdImageDestroy( im );
   gdImageDestroy( present );
}


void linear( opc_sink s, int index, image_zoom_t z )
{
   int dx = move_list[index].ex - move_list[index].sx;
   int dy = move_list[index].ey - move_list[index].sy;
   int d = isqrt( dx*dx + dy*dy );
   float steps = (float)d / (move_list[index].eh + move_list[index].sh);
   int i;
   wait_until_t w = wait_until_init();

//printf( "%f\n", steps );
   if( steps > 120.0 )
   {
      steps = 90.0 + 7.0 * sqrt( steps - 100.0 );
//printf( "->%f\n", steps );
   }
   wait_until_ms( w, 40 );
   image_zoom_calc( z, move_list[index].sx, move_list[index].sy, move_list[index].sh );
   for( i = 0; i <= steps; ++i )
   {
      int x = i * dx / steps + move_list[index].sx;
      int y = i * dy / steps + move_list[index].sy;
      float h = move_list[index].sh;
      image_zoom_calc( z, x, y, h );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
   image_zoom_calc( z, move_list[index].ex, move_list[index].ey, move_list[index].eh );
   wait_until_done( w );
}


void jump( opc_sink s, int index, image_zoom_t z )
{
   int dx = move_list[index].ex - move_list[index].sx;
   int dy = move_list[index].ey - move_list[index].sy;
   float steps = 50.0;
   int i;
   wait_until_t w = wait_until_init();

   wait_until_ms( w, 40 );
   image_zoom_calc( z, move_list[index].sx, move_list[index].sy, move_list[index].sh );
   image_normalize( zim );
   for( i = 0; i < 5; ++i )
   {
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
   for( i = 0; i <= steps; ++i )
   {
      int x = i * dx / steps + move_list[index].sx;
      int y = i * dy / steps + move_list[index].sy;
      float h = sinf( M_PI * i / steps );
      if( i < steps/2 )
      {
         h *= (jump_height - move_list[index].sh);
         h += move_list[index].sh;
      }
      else
      {
         h *= (jump_height - move_list[index].eh);
         h += move_list[index].eh;
      }
      assert( h > 0.0 );
      image_zoom_calc( z, x, y, h );
      image_normalize( zim );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
   image_zoom_calc( z, move_list[index].ex, move_list[index].ey, move_list[index].eh );
   image_normalize( zim );
   for( i = 0; i < 5; ++i )
   {
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
   wait_until_done( w );
}


void jump_out( opc_sink s, int index, image_zoom_t z )
{
   int dx = move_list[index].ex - move_list[index].sx;
   int dy = move_list[index].ey - move_list[index].sy;
   float steps = 50.0;
   int i;
   wait_until_t w = wait_until_init();

   wait_until_ms( w, 40 );
   image_zoom_calc( z, move_list[index].sx, move_list[index].sy, move_list[index].sh );
   image_normalize( zim );
   for( i = 0; i < 12; ++i )
   {
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
   for( i = 0; i <= steps; ++i )
   {
      int x = i * dx / steps + move_list[index].sx;
      int y = i * dy / steps + move_list[index].sy;
      float h = sinf( M_PI_2 * i / steps );
      h *= (move_list[index].eh - move_list[index].sh);
      h += move_list[index].sh;
      assert( h > 0.0 );
      image_zoom_calc( z, x, y, h );
      image_normalize( zim );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
   image_zoom_calc( z, move_list[index].ex, move_list[index].ey, move_list[index].eh );
   image_normalize( zim );
   for( i = 0; i < 12; ++i )
   {
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
   wait_until_done( w );
}



void greetings( opc_sink s, image_zoom_t z, alsaplay_t alsa, int end )
{
   int i;

   wait_until_t w = wait_until_init();

   image_zoom_calc( z, 1024, 512, 64.0 );
   image_normalize( zim );

   wait_until_ms( w, 40 );
   for( i = start; i < (sizeof(move_list)/sizeof(move_list[0])); ++i )
   {
      switch( move_list[i].type )
      {
      case LINEAR:
         linear( s, i, z );
         break;
      case JUMP:
         jump( s, i, z );
         break;
      case JUMP_OUT:
         jump_out( s, i, z );
         break;
      default:
         break;
      }
   }

   image_zoom_calc( z, 1024, 512, 64.0 );
   image_normalize( zim );
   wait_until_ms( w, 40 );
   if( alsaplay_pos_time( alsa ) < end )
   {
      end = alsaplay_pos_time( alsa ) + 5000;
   }
   do
   {
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
   while ( alsaplay_pos_time( alsa ) < end );
   fade_out( s, zim, 100, 40 );
   wait_until_ms( w, 40 );
   gdImageFilledRectangle( zim, 0, 0, zim->sx, zim->sy, 0x000000 );
   while( alsaplay_pos_time( alsa ) < alsaplay_total_time( alsa ) )
   {
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, zim );
   }
}


void _greetings( opc_sink s /*, alsaplay_t alsa, int end*/ )
{
   int i;
   gdImagePtr src;
   image_zoom_t z;

   src = gdImageCreateFromPngPtr( PNG_TRSI_GREETINGS_SIZE, (void*)PNG_TRSI_GREETINGS );
   zim = gdImageCreateTrueColor( 32, 16 );

   z = image_zoom_init( zim, src );
   wait_until_t w = wait_until_init();

   image_zoom_calc( z, 1024, 512, 64.0 );
   image_normalize( zim );

   wait_until_ms( w, 40 );
   for( i = start; i < (sizeof(move_list)/sizeof(move_list[0])); ++i )
   {
      switch( move_list[i].type )
      {
      case LINEAR:
         linear( s, i, z );
         break;
      case JUMP:
         jump( s, i, z );
         break;
      case JUMP_OUT:
         jump_out( s, i, z );
         break;
      default:
         break;
      }
   }

   image_zoom_done( z );
   gdImageDestroy( zim );
   gdImageDestroy( src );
}
