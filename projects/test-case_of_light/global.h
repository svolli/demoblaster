#ifndef GLOBAL_H
#define GLOBAL_H GLOBAL_H

#include <demoblaster.h>

void present_draw( opc_sink s );
void logo_draw( opc_sink s );

void ellipses( opc_sink s, alsaplay_t alsa, int end );

void image_normalize( gdImagePtr im );
void fade_in( opc_sink s, gdImagePtr src, int frames, int delay );
void fade_out( opc_sink s, gdImagePtr src, int frames, int delay );

image_zoom_t trsi_zoom_init();
void trsi_zoom_done( image_zoom_t z );
void trsi_title( opc_sink s, image_zoom_t z, alsaplay_t alsa, int end );
void greetings( opc_sink s, image_zoom_t z, alsaplay_t alsa, int end );

void into_the_blue( opc_sink s, alsaplay_t alsa, int end );

void thank_slide( opc_sink s );
void credits( opc_sink s,
              void *gfx_data, int gfx_size,
              void *txt_data, int txt_size );

void image_sinus_analyzer( opc_sink s, alsaplay_t alsa, int end, void *png_data, int png_size );

void image_sinus( opc_sink s, alsaplay_t alsa, int end,
                  void *png_data, int png_size );
void image_linear( opc_sink s, alsaplay_t alsa, int end,
                   void *png_data, int png_size );
void credits( opc_sink s,
              void *gfx_data, int gfx_size,
              void *txt_data, int txt_size );
void twister(opc_sink s , alsaplay_t alsa, int end);

#endif // GLOBAL_H
