#!/usr/bin/tcc -run -lgd

#include <gd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "../../../libopc/hsv.h"
#include "../../../libopc/hsv_to_rgb.c"
#include "../../../libopc/rgb_to_hsv.c"

#define h2r(x) hsv_to_rgb(x)

#define CP fprintf( stderr, "%s:%d\n", __FILE__, __LINE__ );


static char *fontfile = "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf";
static hsv_t fgcol = { 0.0, 1.0, 1.0 };

const int number_colors = 36;


void set_font( char *filename )
{
   fontfile = filename;
}


void save_destroy( gdImage *im, const char *filename )
{
   FILE *f = fopen( filename, "wb" );

   if( !f )
   {
      perror( filename );
      return;
   }

   gdImagePng( im, f );
   gdImageDestroy( im );
}


gdImage *text2png( double ptsize, double angle, char *text )
{
   static int count = 0;
   char *errmsg = 0;
   gdImage *im = 0;
   int brect[8];
   int x, y;
   int t, b, l ,r;
   angle *= M_PI / 180;

   if( *text == ' ' )
   {
      count--;
      fgcol.h -= (1.0 / number_colors);
      text++;
   }

   printf( "%2d:%s\n", ++count, text );

   errmsg = gdImageStringFT( 0, &brect[0], hsv_to_rgb( &fgcol ), fontfile, ptsize, angle, 0, 0, text );
   if( errmsg )
   {
      fprintf( stderr, "%s\n", errmsg );
      return 0;
   }
   t = brect[5] < brect[7] ? brect[5] : brect[7];
   b = brect[1] > brect[3] ? brect[1] : brect[3];
   l = brect[0] < brect[6] ? brect[0] : brect[6];
   r = brect[2] > brect[4] ? brect[2] : brect[4];

   x = r - l;
   y = b - t;

   im = gdImageCreateTrueColor( x, y );
   gdImageFilledRectangle( im, 0, 0, im->sx-1, im->sy-1, 0 );
   gdImageColorTransparent( im, 0 );

   x = -l;
   y = im->sy - b;
   errmsg = gdImageStringFT( im, &brect[0], hsv_to_rgb( &fgcol ), fontfile, ptsize, angle, x, y, text );
   if( errmsg )
   {
      fprintf( stderr, "%s\n", errmsg );
      return 0;
   }
   fgcol.h += (1.0 / number_colors);
   if( fgcol.h >= 1.0 )
   {
      fgcol.h -= 1.0;
   }

   return im;
}

#define imgcopy( dest, src, x, y ) \
do { \
   if( !src ) fprintf( stderr, "%s=0\n", #src ); \
   gdImageCopy( dest, src, x, y, 0, 0, src->sx, src->sy ); \
   printf( "%2d:(%4d,%4d),(%4d,%4d):%s\n", ++i, x, y, x+src->sx-1, y+src->sy-1, #src ); \
   gdImageDestroy( src ); \
   src = 0; \
} while(0)


void img_normalize( gdImage *im )
{
   int x, y;
   hsv_t col;

   for( y = 0; y < im->sy; ++y )
   {
      for( x = 0; x < im->sx; ++x )
      {
         rgb_to_hsv( &col, im->tpixels[y][x] );
         if( col.v > 0.01 )
         {
            col.v = 1.0;
            im->tpixels[y][x] = hsv_to_rgb( &col );
         }
      }
   }
}


int main( int argc, char *argv[] )
{
   FILE *f;
   int i = 0;
   const double pt = 36.0;

   gdImage *im = gdImageCreateTrueColor( 2048, 1024 );
   gdImage *pv = gdImageCreateTrueColor( 32, 16 );

   gdImage *CodeRed                       = text2png( pt,   0, "CODE RED" );
   gdImage *greetings                     = text2png( pt,   0, "GREETINGS FLY OUT TO:" );
   gdImage *AndromedaSoftwareDevelopment  = text2png( pt, -90, "ANDROMEDA SOFTWARE DEVELOPMENT" );
   gdImage *DigitalDemolitionKrew         = text2png( pt,  90, "DIGITAL DEMOLITION KREW" );
   gdImage *DigitalSoundSystem            = text2png( pt, -90, "DIGITAL SOUND SYSTEM" );
   gdImage *AbyssConnection               = text2png( pt, -70, "ABYSS CONNECTION" );
   gdImage *Genesis                       = text2png( pt,   0, "GENESIS*PROJECT" );
   gdImage *TheBlackLotus                 = text2png( pt,   0, "THE BLACK LOTUS" );
   gdImage *BrainControl                  = text2png( pt,  90, "BRAIN CONTROL" );
   gdImage *BoozeDesign                   = text2png( pt, -90, "BOOZE DESIGN" );
   gdImage *PandaDesign                   = text2png( pt,  90, "PANDA DESIGN" );
   gdImage *Conspiracy                    = text2png( pt,  90, "CONSPIRACY" );
   gdImage *Farbrausch                    = text2png( pt,   0, "FARBRAUSCH" );
   gdImage *TheDreams                     = text2png( pt,  90, "THE DREAMS" );
   gdImage *Dekadence                     = text2png( pt, -70, "DEKADENCE" );
   gdImage *rtificial                     = text2png( pt,  70, "RTIFICIAL" );
   gdImage *Onslaught                     = text2png( pt,   0, "ONSLAUGHT" );
   gdImage *Rabenauge                     = text2png( pt,   0, "RABENAUGE" );
   gdImage *Ghostown                      = text2png( pt, -90, "GHOSTOWN" );
   gdImage *Mercury                       = text2png( pt, -60, "MERCURY" );
   gdImage *Gaspode                       = text2png( pt, -60, "GASPODE" );
   gdImage *Arsenic                       = text2png( pt,   0, "ARSENIC" );
   gdImage *Cluster                       = text2png( pt,  90, "CLUSTER" );
   gdImage *Haujobb                       = text2png( pt, -60, "HAUJOBB" );
   gdImage *Hitmen                        = text2png( pt,   0, "HITMEN" );
   gdImage *Kaomau                        = text2png( pt,   0, "KAOMAU" );
   gdImage *Nuance                        = text2png( pt,  60, "NUANCE" );
   gdImage *Oxyron                        = text2png( pt, -60, "OXYRON" );
   gdImage *SVatG                         = text2png( pt,   0, "SVATG" );
   gdImage *Titan                         = text2png( pt, -90, "TITAN" );
   gdImage *TRBL                          = text2png( pt,   0, "TRBL" );
   gdImage *MEGA                          = text2png( pt,   0, "MEGA" );
   gdImage *lft                           = text2png( pt,  00, "LFT" );
   gdImage *Miss                          = text2png( pt,  60, "MISS" );
   gdImage *Ctrl                          = text2png( pt, -60, " CTRL" );
   gdImage *Akronyme                      = text2png( pt,  60, "AKRONYME" );
   gdImage *Analogiker                    = text2png( pt,   0, " ANALOGIKER" );
   gdImage *AllVCSCoders                  = text2png( pt, -90, "ALL VCS CODERS" );

   {
      gdImage *t = gdImageCreateTrueColor( Ctrl->sx, Ctrl->sy);
      gdImageCopyRotated( t, Ctrl, t->sx / 2, t->sy / 2, 0, 0, Ctrl->sx, Ctrl->sy, 180 );
      gdImageDestroy( Ctrl );
      Ctrl = t;
   }

   gdImageColorTransparent( im, 0x000000 );

   puts( "  T");
   imgcopy( im, greetings, 0, 0 );
   imgcopy( im, TheBlackLotus, 60, 60 );
   imgcopy( im, DigitalDemolitionKrew, 300, 140 );
   imgcopy( im, Titan, 300, 870 );
   imgcopy( im, BoozeDesign, 230, 140 );
   imgcopy( im, lft, 200, 550 );
   imgcopy( im, PandaDesign, 230, 640 );

   puts( "  R");
   imgcopy( im, AndromedaSoftwareDevelopment, 640, 40 );
   imgcopy( im, Conspiracy, 700, 120 );
   imgcopy( im, Arsenic, 720, 60 );
   imgcopy( im, BrainControl, 700, 600 );
   imgcopy( im, Rabenauge, 690, 00 );
   imgcopy( im, Gaspode, 1000, 20 );
   imgcopy( im, Miss, 980, 250 );
   imgcopy( im, Ctrl, 980, 120 );
   imgcopy( im, Nuance,  1000, 260 );
   imgcopy( im, CodeRed, 740, 450 );
   imgcopy( im, Kaomau, 700, 510 );
   imgcopy( im, AbyssConnection, 920, 540 );
   imgcopy( im, Dekadence, 910, 720 );

   puts( "  S");
   imgcopy( im, Akronyme, 1300, 20 ); i--;
   imgcopy( im, Analogiker, 1455, 0 );
   imgcopy( im, Onslaught, 1470, 60 );
   imgcopy( im, Oxyron, 1330, 270 );
   imgcopy( im, Mercury, 1380, 210 );
   imgcopy( im, Hitmen, 1460, 450 );
   imgcopy( im, SVatG, 1480, 510 );
   imgcopy( im, Ghostown, 1630, 580 );
   imgcopy( im, Haujobb, 1640, 480 );
   imgcopy( im, rtificial, 1670, 720 );
   imgcopy( im, Farbrausch, 1300, 900 );
   imgcopy( im, Genesis, 1280, 970 );

   puts( "  I");
   imgcopy( im, TRBL, 1900, 0 );
   imgcopy( im, MEGA, 1900, 50 );
   imgcopy( im, DigitalSoundSystem, 1990, 190 );
   imgcopy( im, Cluster, 1990, 800 );
   imgcopy( im, TheDreams, 1920, 190 );
   imgcopy( im, AllVCSCoders, 1920, 595 );


   f = fopen( "greetings.png", "wb" );
   gdImagePng( im, f );
   fclose( f );

   gdImageCopyResampled( pv, im, 0, 0, 0, 0, pv->sx, pv->sy, im->sx, im->sy );
   img_normalize( pv );
   f = fopen( "preview.png", "wb" );
   gdImagePng( pv, f );
   fclose( f );


   return 0;
}
