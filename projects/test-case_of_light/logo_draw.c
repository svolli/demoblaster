
#include <demoblaster.h>

#include "media/logo_draw.h"

#include <math.h>


void logo_draw( opc_sink s )
{
   const int brighten = 24;
   const int dots = sizeof(LOGO_DRAW)/sizeof(LOGO_DRAW[0]);
   int i, j, x, y;
   float f;
   hsv_t color;
   gdImagePtr tmp = gdImageCreateTrueColor( 32, 16 );
   gdImagePtr im = gdImageCreateTrueColor( 32, 16 );
   wait_until_t w = wait_until_init();

   color.s = 1.0;

   wait_until_ms( w, 40 );

   gdImageFilledRectangle( im, 0, 0, im->sx-1, im->sy-1, 0x000000 );
   for( i = 0; i < dots + brighten; ++i )
   {
      color.h = (float)i / dots - 0.08;
      if( color.h < 0.0 )
      {
         color.h += 1.0;
      }
      for( j = 1; j < brighten; ++j )
      {
         int idx = i - j;
         if( idx < 0 )
         {
            break;
         }
         if( idx >= dots )
         {
            continue;
         }
         if( LOGO_DRAW[idx] < 0 )
         {
            continue;
         }
         x = LOGO_DRAW[idx] % 32;
         y = LOGO_DRAW[idx] / 32;
         color.v = (float)j / brighten;
         gdImageSetPixel( im, x, y, hsv_to_rgb( &color ) );
      }
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );
   }

   for( i = 0; i < 25; ++i )
   {
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );
   }

   for( f = 0.0; f <= 1.0; f += 1.0/25 )
   {
      int h = cosf( M_PI_2 * f ) * 15;
      gdImageFilledRectangle( tmp, 0, 0, tmp->sx-1, tmp->sy-1, 0x000000 );

      gdImageCopyResampled( tmp, im, 0, 15 - h, 0, 0,
                            tmp->sx, 1 + h, im->sx, im->sy );

      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, tmp );
   }

   gdImageDestroy( tmp );
   gdImageDestroy( im );
   wait_until_done( w );
}
