#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <demoblaster.h>
#include <math.h>

#include <fftw3.h>

#define FFT_BLOCKSIZE (4096)
#define FFT_CHUNKSIZE (2048)

fftwf_complex *out_cpx;
fftwf_plan fft;
float bdata[FFT_BLOCKSIZE];
float fdata[FFT_BLOCKSIZE];
float spectrum[FFT_BLOCKSIZE/2+1];
int offset = 0;
const int binidx[32] = { 0, 1, 2, 3, 4, 5, 7, 9, 11, 13, 16, 19, 23, 31, 39, 47, 63, 79, 95, 127, 159, 191, 255, 319, 383, 511, 639, 767, 1023, 1279, 1535, 65536 };
float bins[32];


void visualize( char* data, int size )
{
   int i, idx;
   int16_t *audio = (int16_t*)data;

   if( size != FFT_CHUNKSIZE * 4 )
   {
      printf( "visualize: illegal size: %d\n", size );
   }

   for( i = 0; i < FFT_CHUNKSIZE; ++i )
   {
      bdata[offset+i] = ((float)(audio[i*2+0] + audio[i*2+1])) / 65536;
   }
   offset += FFT_CHUNKSIZE;
   if( offset >= FFT_BLOCKSIZE )
   {
      offset -= FFT_BLOCKSIZE;
   }

   memcpy( fdata, bdata, sizeof( fdata ) );
   fftwf_execute( fft );

   for( i = 0; i < 32; ++i )
   {
      /* todo: move cleaning bins after reading data for better async */
      bins[i] = 0.0;
   }

   idx = 0;
   for( i = 0; i < 512; ++i )
   {
      int x;
      //float f = 0.0;
      for( x = 0; x < (FFT_BLOCKSIZE/512/2); ++x )
      {
         int n = i*(FFT_BLOCKSIZE/512/2)+x;
         bins[idx] += out_cpx[n][0]*out_cpx[n][0] + out_cpx[n][1]*out_cpx[n][1];
         if( n > binidx[idx] )
         {
            idx++;
         }
      }
   }
}


void image_sinus_analyzer( opc_sink s, alsaplay_t alsa, int end, void *png_data, int png_size )
{
   gdImagePtr im, scaled, analyzer;
   float sinpos = M_PI_2 + M_PI;
   int i, p = 0, xsize, ysize, xmove, ymove, xoffset, yoffset;
   wait_until_t w = wait_until_init();

   out_cpx = fftwf_alloc_complex( FFT_BLOCKSIZE/2+1 );
   fft = fftwf_plan_dft_r2c_1d( FFT_BLOCKSIZE, fdata, out_cpx, FFTW_ESTIMATE );  //Setup fftw plan for fft 1 dimensional, real signal

   alsaplay_set_tap( alsa, visualize );
   im = gdImageCreateFromPngPtr( png_size, png_data );
   if( !im )
   {
      FAIL();
   }
   scaled   = gdImageCreateTrueColor( 32, 16 );
   analyzer = gdImageCreateTrueColor( 32, 16 );
   if( (scaled->sx * im->sy) == (im->sx * scaled->sy) )
   {
      /* special case: no movement needed */

      opc_put_gd_image_32x16z( s, im );
      return;
   }
   if( (scaled->sx * im->sy) < (im->sx * scaled->sy) )
   {
      xsize = im->sy * scaled->sx / scaled->sy;
      ysize = im->sy;
      xmove = im->sx - xsize;
      ymove = 0;
   }
   else
   {
      xsize = im->sx;
      ysize = im->sx * scaled->sy / scaled->sx;
      xmove = 0;
      ymove = im->sy - ysize;
   }

   wait_until_ms( w, 40 );
   while( alsaplay_pos_time( alsa ) < end )
   {
      sinpos += 0.033;
      if( sinpos >= M_PI * 2 )
      {
         sinpos -= M_PI * 2;
      }

      xoffset = (sinf(sinpos)+1) * xmove / 2;
      yoffset = (sinf(sinpos)+1) * ymove / 2;
      gdImageCopyResampled( scaled, im,
                            0, 0, xoffset, yoffset,
                            32, 16, xsize, ysize );
      gdImageCopy( analyzer, scaled, 0, 0, 0, 0, scaled->sx, scaled->sy );

      for( i = 0; i < 32; ++i )
      {
         int y;
         int v = (int)logf(bins[i] * 100);
         int c = v + 3;
         for( y = 0; y < 16; ++y )
         {
            int color = 0;
            if( v >= y )
            {
               int mul = isqrt( c * v );
               if( mul > 0xf )
               {
                  mul = 0xf;
               }
               color = y > 12 ? 0x100000 : 0x001000;
               color *= mul;
               if( --c < 1 )
               {
                  c = 1;
               }
            }
            if( color )
            {
               gdImageSetPixel( analyzer, i, 15-y, color );
            }
         }
      }
      if( ++p >= 200 )
      {
         p = 0;
      }
      if( p < 100 )
      {
         gdImageCopyMerge( scaled, analyzer, 0, 0, 0, 0, scaled->sx, scaled->sy, p );
      }
      else
      {
         gdImageCopyMerge( scaled, analyzer, 0, 0, 0, 0, scaled->sx, scaled->sy, 200 - p );
      }

      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, scaled );
   }

   gdImageDestroy( im );
   gdImageDestroy( scaled );
   wait_until_done( w );
   alsaplay_set_tap( alsa, 0 );
   fftwf_destroy_plan( fft );
   fftwf_free( out_cpx );
}

