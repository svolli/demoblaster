
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>

#include "global.h"
#include "media/into_the_blue.h"


void into_the_blue( opc_sink s, alsaplay_t alsa, int end )
{
   int xpos = 0;
   int ypos = 0;
   int xdir = 1;
   int ydir = 1;
   int i = 0;
   float sinpos = 0.0;
   float height = 2.1;
   gdImagePtr im, src;
   image_zoom_t z = 0;
   wait_until_t w = wait_until_init();

   src = gdImageCreateFromPngPtr( INTO_THE_BLUE_SIZE, (void*)INTO_THE_BLUE );
   xpos = src->sx / 2 - 10;
   ypos = src->sy / 2;
   im = gdImageCreateTrueColor( 32, 16 );
   if( !src )
   {
      perror( "opening file" );
      exit( EXIT_FAILURE );
   }
   z = image_zoom_init( im, src );
   image_zoom_calc( z, xpos, ypos, height );
   fade_in( s, im, 25, 40 );

   wait_until_ms( w, 40 );
   for( i = 0; i < 530; ++i )
   {
      sinpos += 0.1;
      if( sinpos >= M_PI * 2 )
      {
         sinpos -= M_PI * 2;
      }
      height = (sinf(sinpos) + 1.0) + 1.1;

      xpos += xdir;
      ypos += ydir;

      if( xpos >= (src->sx - im->sx) )
      {
         xdir = -abs(xdir);
      }
      if( xpos < im->sx )
      {
         xdir = abs(xdir);
      }
      if( ypos >= (src->sy - im->sy) )
      {
         ydir = -abs(ydir);
      }
      if( ypos < im->sy )
      {
         ydir = abs(ydir);
      }

      image_zoom_calc( z, xpos, ypos, height );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );
   }

   sinpos += 0.9;
   for( i = 0; i < 130; ++i )
   {
      sinpos += 0.1;
      if( sinpos >= M_PI * 2 )
      {
         sinpos -= M_PI * 2;
      }
      height = 3 * (sinf(sinpos) + 1.0) + 1.1;
      image_zoom_calc( z, xpos, ypos, height );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );
   }
   while( alsaplay_pos_time( alsa ) < (end - 1000) )
   {
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );
   }
   fade_out( s, im, 25, 40 );

   gdImageDestroy( im );
   gdImageDestroy( src );
   image_zoom_done( z );
   wait_until_done( w );
}
