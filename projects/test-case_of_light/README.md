 Test-Case Of Light
====================

Compiling
---------
Please take note, that compiling and linking requires about 1.5G of memory.
This is more than a stock Raspberry Pi 3B, the machine this was written for,
is equipped with. As a workaround, a swap partition (or file) with a size of
1G is strongly recommended.

Luckily, there is a shell script `swap.sh` included that will generate,
activate and deactivate a required swapfile during the build process.

