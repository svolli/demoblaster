
#include "global.h"


void image_normalize( gdImagePtr im )
{
   int x, y;
   hsv_t col;
   float v = 0.0;

   for( y = 0; y < im->sy; ++y )
   {
      for( x = 0; x < im->sx; ++x )
      {
         rgb_to_hsv( &col, im->tpixels[y][x] );
         if( col.v > v )
         {
            v = col.v;
            im->tpixels[y][x] = hsv_to_rgb( &col );
         }
      }
   }
   v = 1/v;
   for( y = 0; y < im->sy; ++y )
   {
      for( x = 0; x < im->sx; ++x )
      {
         rgb_to_hsv( &col, im->tpixels[y][x] );
         if( col.v > 0.01 )
         {
            col.v *= v;
            im->tpixels[y][x] = hsv_to_rgb( &col );
         }
      }
   }
}


static void fader( opc_sink s, gdImagePtr src, int delay, int start, int end, int direction )
{
   int i = 0, f = 0;
   int x = 0, y = 0;
   int frames = start > end ? start : end;
   gdImagePtr im = gdImageCreateTrueColor( src->sx, src->sy );
   wait_until_t w = wait_until_init();

   wait_until_ms( w, delay );

   for( f = start; f != end; f += direction )
   {
      for( y = 0; y < im->sy; ++y )
      {
         for( x = 0; x < im->sx; ++x )
         {
            int ocol = gdImageGetTrueColorPixel( src, x, y );
            int ncol = 0;

            for( i = 0; i < 3; ++i )
            {
               int c = ocol & (0xFF << (i*8));
               c *= f;
               c /= frames;
               c &= (0xFF << (i*8));
               ncol |= c;
            }
            gdImageSetPixel( im, x, y, ncol );
         }
      }
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );
   }
}


void fade_in( opc_sink s, gdImagePtr src, int frames, int delay )
{
   fader( s, src, delay, 0, frames - 1, +1 );
}


void fade_out( opc_sink s, gdImagePtr src, int frames, int delay )
{
   fader( s, src, delay, frames - 1, 0, -1 );
}
