
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "global.h"


void ellipses(opc_sink s, alsaplay_t alsa, int end )
{
   const int oversampling = 8;
   const int size_base    = 4 * oversampling;
   const int size_stretch = oversampling;

   int frame = 0;
   gdImagePtr im = 0, col = 0;
   float sinposx = 0.0;
   float sinposy = 0.0;
   int x = 0, y = 0;
   int destx = 0, desty = 0;
   hsv_t color1 = { 0.0, 1.0, 1.0 };
   hsv_t color2 = { 0.5, 1.0, 1.0 };
   wait_until_t w = wait_until_init();
   im = gdImageCreateTrueColor( 32, 16 );
   ellipses_t ellipses = ellipses_init( im, oversampling );

   x = im->sx * oversampling / 2;
   y = im->sy * oversampling / 2;
   destx = x;
   desty = y;
   col = gdImageCreateTrueColor( 32, 16 );

   wait_until_ms( w, 40 );
   for( frame = 0; alsaplay_pos_time( alsa ) < end; ++frame )
   {
      if( x < destx )
      {
         ++x;
      }
      else if( x > destx )
      {
         --x;
      }
      else
      {
         if( frame >= 400 )
         {
            destx = rand() % (im->sx * oversampling / 2) + (im->sx * oversampling / 4);
         }
      }
      if( y < desty )
      {
         ++y;
      }
      else if( y > desty )
      {
         --y;
      }
      else
      {
         if( frame >= 400 )
         {
            desty = rand() % (im->sy * oversampling / 2) + (im->sy * oversampling / 4);
         }
      }
      ellipses_set_calc( ellipses, /*im->sx * oversampling / 2, im->sy * oversampling / 2,*/ x, y,
                         sinf(sinposx) * size_stretch + size_base, cosf(sinposy) * size_stretch + size_base,
//                         hsv_to_rgb( &color1 ), hsv_to_rgb( &color2 ) );
                         0xFFFFFF, 0 );

      if( frame >= 200 )
      {
         int ix, iy;
         hsv_t c = color1;
         for( ix = 0; ix < col->sx; ++ix )
         {
            //int rgb = hsv_to_rgb( &c );
            hsv_t c2 = c;
            c.h += 4.0 / 360;
            if( c.h >= 1.0 )
            {
               c.h -= 1.0;
            }
            for( iy = 0; iy < col->sy; ++iy )
            {
               int rgb = hsv_to_rgb( &c2 );
               c2.h += 4.0 / 360;
               if( c2.h >= 1.0 )
               {
                  c2.h -= 1.0;
               }
               gdImageSetPixel( col, ix, iy, rgb );
            }
         }
         gdImageCopyMultiply( im, col, 0, 0, 0, 0, im->sx, im->sy );
      }
      color1.h += 2.0 / 360;
      if( color1.h >= 1.0 )
      {
         color1.h -= 1.0;
      }
      color2.h += 2.0 / 360;
      if( color2.h >= 1.0 )
      {
         color2.h -= 1.0;
      }
      sinposx += 0.11;
      if( sinposx > M_PI * 2 )
      {
         sinposx -= M_PI * 2;
      }
      sinposy += 0.09;
      if( sinposy > M_PI * 2 )
      {
         sinposy -= M_PI * 2;
      }
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );
   }

   wait_until_done( w );
   gdImageDestroy( im );
   gdImageDestroy( col );
}
