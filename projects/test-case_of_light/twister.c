
#include <math.h>
#include <stdlib.h>

#include "global.h"

void twister( opc_sink s, alsaplay_t alsa, int end )
{
   gdImagePtr im = 0;
   wait_until_t w = 0;
   twister_t t = 0;
   int i = 0, frame = 0;
   int direction = 1;
   int horizontal = 0;
   float twist = 0.0;
   hsv_t color[2];

   for( i = 0; i < 2; ++i )
   {
      color[i].h = 120.0 * i / 360;
      color[i].s = 1.0;
      color[i].v = 1.0;
   }

   im = gdImageCreateTrueColor( 32, 16 );
   t = twister_init( im, 3 );
   w = wait_until_init();

   wait_until_ms( w, 40 );
   for( frame = 0; alsaplay_pos_time( alsa ) < end; ++frame )
   {
      twister_set_twist( t, horizontal & 1, twist );
      twister_set_colors( t, &(color[0]), &(color[1]) );
      twister_calc( t );
      wait_until_timeout( w );
      opc_put_gd_image_32x16z( s, im );

      switch( frame / 100 )
      {
      case 11:
      case 10:
      case 9:
      case 8:
         horizontal = 1;
      case 7:
      case 6:
         for( i = 0; i < 2; ++i )
         {
            color[i].h += 3.0 / 360;
            while( color[i].h >= 1.0 )
            {
               color[i].h -= 1.0;
            }
         }
         /* slip through */
      case 5:
      case 4:
         twist += direction * 0.002;
         if( twist >= 0.2 )
         {
            direction = -1;
         }
         if( twist <= 0.0 )
         {
            direction = 1;
         }
         break;
      case 3:
         twist -= 0.0005;
         break;
      case 1:
         twist += 0.0005;
         break;
      case 2:
      case 0:
      default:
         break;
      }
   }
   gdImageFilledRectangle( im, 0, 0, im->sx-1, im->sy-1, 0x000000 );
   wait_until_timeout( w );
   opc_put_gd_image_32x16z( s, im );

   wait_until_done( w );
   twister_done( t );
   gdImageDestroy( im );
}
