
#include <alsa/asoundlib.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "alsaplay.h"
#include "tremor_in_ram.h"


#define BUFFERSIZE (8192)

#if 1
static char *device = "default";    /* playback device */
#else
static char *device = "plughw:0,0"; /* playback device */
#endif

struct alsaplay_s
{
   OggVorbis_File    *vf;
   snd_pcm_t         *handle;
   char              *buffer;
   pthread_t         thread_id;
   alsaplay_time_t   pcm_pos;
   alsaplay_time_t   pcm_seek;
   alsaplay_time_t   pcm_total;
   alsaplay_time_t   time_pos;
   alsaplay_time_t   time_total;
   alsaplay_time_t   loop_start;
   alsaplay_time_t   loop_end;
   int               sample_size;
   int               done;
   int               loop;
   alsaplay_tap_t    tap;
};


alsaplay_t alsaplay_init( OggVorbis_File *vf )
{
   alsaplay_t alsa = (alsaplay_t)calloc( 1, sizeof( *alsa ) );
   
   alsa->pcm_seek = -1;
   alsa->vf       = vf;
   alsa->buffer   = (char*)malloc( BUFFERSIZE );
   
   return alsa;
}


int alsaplay_file( alsaplay_t alsa, const char *filename )
{
   int err;
   err = ov_load_file( filename, alsa->vf );
   if( !err )
   {
      alsaplay_setup( alsa, alsa->vf );
   }
   return err;
}


void alsaplay_setup( alsaplay_t alsa, OggVorbis_File *vf )
{
   int err;
   vorbis_info *vi;

   /* zero out all value */
   alsa->vf          = vf;

   __sync_synchronize();
   alsa->pcm_total   = (alsaplay_time_t)ov_pcm_total( alsa->vf, -1 );
   alsa->time_total  = (alsaplay_time_t)ov_time_total( alsa->vf, -1 );
   __sync_synchronize();

   if( !alsa->buffer )
   {
      perror( "malloc" );
      exit( EXIT_FAILURE );
   }

   vi = ov_info( alsa->vf, -1 );

   if( (err = snd_pcm_open( &(alsa->handle), device, SND_PCM_STREAM_PLAYBACK, 0 )) < 0 )
   {
      printf( "Playback open error: %s\n", snd_strerror( err ) );
      exit(EXIT_FAILURE);
   }

   alsa->sample_size = vi->channels * 2;
   if( (err = snd_pcm_set_params( alsa->handle,
                                  SND_PCM_FORMAT_S16_LE, /* format */
                                  SND_PCM_ACCESS_RW_INTERLEAVED, /* access */
                                  vi->channels,      /* channels */
                                  vi->rate,          /* samplerate */
                                  1,                 /* resample */
                                  50000) ) < 0 )     /* 50ms latency */
   {
      printf( "Playback open error: %s\n", snd_strerror( err ) );
      exit( EXIT_FAILURE );
   }
}


static void *alsaplay_thread( void *payload )
{
   volatile alsaplay_t alsa = (alsaplay_t)payload;
   int err;
   int done = 0;

   while( !done )
   {
      int bytes_left = BUFFERSIZE;
      char *readpos = alsa->buffer;
      
      if( __sync_fetch_and_add( &(alsa->pcm_seek), 0 ) >= 0 )
      {
         __sync_synchronize();
         ov_pcm_seek( alsa->vf, alsa->pcm_seek );
         alsa->pcm_seek = -1;
         __sync_synchronize();
      }

      done = __sync_fetch_and_add( &(alsa->done), 0 );

      while( bytes_left > 0 )
      {
         int bytes_decoded = ov_read( alsa->vf, readpos, bytes_left, 0 );
         if( bytes_decoded < 0 )
         {
            /* read error */
            perror( "ov_read" );
            exit( EXIT_FAILURE );
         }
         else if( !bytes_decoded )
         {
            /* EOF */
            if( alsa->loop )
            {
               /* rewind */
               ov_raw_seek( alsa->vf, 0 );
            }
            else
            {
               /* fill with 0 */
               memset( readpos, 0, bytes_left );
               bytes_decoded = bytes_left;
            }
         }

         readpos    += bytes_decoded;
         bytes_left -= bytes_decoded;
      }

      err = snd_pcm_writei( alsa->handle, alsa->buffer, BUFFERSIZE / alsa->sample_size );
      if( err == -EAGAIN )
      {
         continue;
      }
      if( err < 0 )
      {
         err = snd_pcm_recover( alsa->handle, err, 0 );
      }
      if( err < 0 )
      {
         printf( "snd_pcm_writei failed: %s\n", snd_strerror( err ) );
         break;
      }

      __sync_synchronize();
      alsa->pcm_pos  = (alsaplay_time_t)ov_pcm_tell( alsa->vf );
      alsa->time_pos = (alsaplay_time_t)ov_time_tell( alsa->vf );
      if( alsa->tap )
      {
         alsa->tap( alsa->buffer, BUFFERSIZE );
      }
#if 0
      alsaplay_time_t bufstart = ov_pcm_tell( alsa->vf );
      alsaplay_time_t bufend   = bufstart + BUFFERSIZE / alsa->sample_size;
      alsaplay_time_t cb_time  = 0;
      if( (cb_time > 0) && (cb_time >= bufstart) && (cb_time < bufend) )
      {
         cb_callback( alsa );
      }
#endif
      if( (alsa->loop_end > 0) && (alsa->pcm_pos >= alsa->loop_end) )
      {
         ov_pcm_seek( alsa->vf, alsa->loop_start + alsa->pcm_pos - alsa->loop_end );
      }
      alsa->pcm_pos  = (alsaplay_time_t)ov_pcm_tell( alsa->vf );
      alsa->time_pos = (alsaplay_time_t)ov_time_tell( alsa->vf );
      __sync_synchronize();
   }

   return 0;
}


void alsaplay_start( alsaplay_t alsa )
{
   int err;
   pthread_attr_t attr;
   
   alsa->done = 0;

   err = pthread_attr_init( &attr );
   if( err )
   {
      perror( "pthread_attr_init" );
      exit( EXIT_FAILURE );
   }

   err = pthread_create( &(alsa->thread_id), &attr, alsaplay_thread, alsa );
   if( err )
   {
      perror( "pthread_attr_init" );
      exit( EXIT_FAILURE );
   }

   err = pthread_attr_destroy( &attr );
   if( err )
   {
      perror( "pthread_attr_destroy" );
      exit( EXIT_FAILURE );
   }
}


void alsaplay_seek( alsaplay_t alsa, alsaplay_time_t pos )
{
   alsa->pcm_seek = pos;
}


void alsaplay_set_loop( alsaplay_t alsa, alsaplay_time_t start, alsaplay_time_t end )
{
   alsa->loop_start = start;
   alsa->loop_end   = end;
}


void alsaplay_done( alsaplay_t alsa )
{
   alsaplay_stop( alsa );
   free( alsa->buffer );
   free( alsa );
}


alsaplay_time_t alsaplay_pos_sample( alsaplay_t alsa )
{
   return __sync_fetch_and_add( &(alsa->pcm_pos), 0 );
}


alsaplay_time_t alsaplay_pos_time( alsaplay_t alsa )
{
   return __sync_fetch_and_add( &(alsa->time_pos), 0 );
}


alsaplay_time_t alsaplay_total_sample( alsaplay_t alsa )
{
   return __sync_fetch_and_add( &(alsa->pcm_total), 0 );
}


alsaplay_time_t alsaplay_total_time( alsaplay_t alsa )
{
   return __sync_fetch_and_add( &(alsa->time_total), 0 );
}


void alsaplay_set_tap( alsaplay_t alsa, alsaplay_tap_t tap )
{
   __sync_synchronize();
   alsa->tap = tap;
   __sync_synchronize();
}


void alsaplay_stop( alsaplay_t alsa )
{
   int err;
   __sync_synchronize();
   if( alsa->done )
   {
      return;
   }
   alsa->done = 1;
   __sync_synchronize();
   err = pthread_join( alsa->thread_id, 0 );
   if( err )
   {
      perror( "pthread_join" );
   }
   snd_pcm_close( alsa->handle );
   ov_clear( alsa->vf );
}


OggVorbis_File *alsaplay_vf( alsaplay_t alsa )
{
   return alsa->vf;
}
