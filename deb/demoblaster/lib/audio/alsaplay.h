
#ifndef ALSAPLAY_H
#define ALSAPLAY_H ALSAPLAY_H

#include <tremor/ivorbisfile.h>
#include <alsa/asoundlib.h>
#include <pthread.h>

typedef int32_t alsaplay_time_t;

typedef struct alsaplay_s *alsaplay_t;
typedef void (*alsaplay_tap_t)( char* buffer, int size );

alsaplay_t alsaplay_init( OggVorbis_File *vf );
int  alsaplay_file( alsaplay_t alsa, const char *filename );
void alsaplay_setup( alsaplay_t alsa, OggVorbis_File *vf );
void alsaplay_start( alsaplay_t alsa );
void alsaplay_stop( alsaplay_t alsa );
void alsaplay_set_tap( alsaplay_t alsa, alsaplay_tap_t tap );
void alsaplay_seek( alsaplay_t alsa, alsaplay_time_t pos );
void alsaplay_set_loop( alsaplay_t alsa, alsaplay_time_t start, alsaplay_time_t end );
void alsaplay_done( alsaplay_t alsa );

alsaplay_time_t alsaplay_pos_sample( alsaplay_t alsa );
alsaplay_time_t alsaplay_pos_time( alsaplay_t alsa );
alsaplay_time_t alsaplay_total_sample( alsaplay_t alsa );
alsaplay_time_t alsaplay_total_time( alsaplay_t alsa );
OggVorbis_File *alsaplay_vf( alsaplay_t alsa );

#endif
