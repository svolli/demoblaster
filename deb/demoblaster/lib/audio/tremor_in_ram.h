
#ifndef TREMOR_IN_RAM_H
#define TREMOR_IN_RAM_H TREMOR_IN_RAM_H

#include <tremor/ivorbisfile.h>

int ov_load_file( const char *filename, OggVorbis_File *vf );
int ov_load_data( const char *data, size_t size, OggVorbis_File *vf );

#endif
