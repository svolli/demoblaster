
#include "gd_db.h"

#include <string.h>
#include <gd.h>


gdImagePtr gdImageLoadFile( const char *filename )
{
   FILE *f;
   char buffer[16];
   gdImagePtr im = 0;
   
   f = fopen( filename, "rb" );
   if( !f )
   {
      return 0;
   }
   if( fread( &buffer[0], sizeof( buffer), 1, f ) < 1 )
   {
      fclose( f );
      return 0;
   }
   rewind( f );
   if( !strncmp( &buffer[1], "PNG", 3 ) )
   {
      im = gdImageCreateFromPng( f );
   }
   else if(( !strncmp( &buffer[6], "JFIF", 4 ) )
        || ( !strncmp( &buffer[6], "Exif", 4 ) ))
   {
      im = gdImageCreateFromJpeg( f );
   }
   else if( !strncmp( &buffer[0], "GIF8", 4 ) )
   {
      im = gdImageCreateFromGif( f );
   }
   fclose( f );
   return im;
}

