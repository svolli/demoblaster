
#include "gd_db.h"

/* this is taken from gdImageCopyMerge and slightly edited */
void gdImageCopyMultiply( gdImagePtr dst, gdImagePtr src, int dstX, int dstY,
                          int srcX, int srcY, int w, int h )
{
   int c, dc;
   int x, y;
   int tox, toy;
   int ncR, ncG, ncB;
   toy = dstY;
   for( y = srcY; y < (srcY + h); y++)
   {
      tox = dstX;
      for( x = srcX; x < (srcX + w); x++)
      {
         int nc;
         c = gdImageGetPixel( src, x, y);
         /* Added 7/24/95: support transparent copies */
         if( gdImageGetTransparent( src) == c)
         {
            tox++;
            continue;
         }
         /* If it's the same image, mapping is trivial */
         if( dst == src )
         {
            nc = c;
         }
         else
         {
            dc = gdImageGetPixel( dst, tox, toy );

#if 1
            /* fast version */
            ncR = (gdImageRed(src, c)   * gdImageRed(dst, dc))   >> 8;
            ncG = (gdImageGreen(src, c) * gdImageGreen(dst, dc)) >> 8;
            ncB = (gdImageBlue(src, c)  * gdImageBlue(dst, dc))  >> 8;
#else
//          /* accurate version */
            ncR = gdImageRed(src, c)   * gdImageRed(dst, dc)   / 0xFF;
            ncG = gdImageGreen(src, c) * gdImageGreen(dst, dc) / 0xFF;
            ncB = gdImageBlue(src, c)  * gdImageBlue(dst, dc)  / 0xFF;
#endif

            /* Find a reasonable color */
            nc = gdImageColorResolve( dst, ncR, ncG, ncB );
         }
         gdImageSetPixel( dst, tox, toy, nc);
         tox++;
      }
      toy++;
   }
}

