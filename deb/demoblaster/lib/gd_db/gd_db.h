
#ifndef GDEXT_H
#define GDEXT_H GDEXT_H

#include <gd.h>
#include <gdfontg.h>
#include <gdfontl.h>
#include <gdfontmb.h>
#include <gdfonts.h>
#include <gdfontt.h>
#include "gdfont4x8.h"
#include "gdfont6x8apple1.h"
#include "gdfont8x8.h"

/* gdImageCopyAdd.c */
void gdImageCopyAdd( gdImagePtr dst, gdImagePtr src, int dstX, int dstY,
                     int srcX, int srcY, int w, int h );
/* gdImageCopyMultiply.c */
void gdImageCopyMultiply( gdImagePtr dst, gdImagePtr src, int dstX, int dstY,
                          int srcX, int srcY, int w, int h );
/* gdImageCreateFromText.c */
gdImagePtr gdImageCreateFromText(gdFontPtr font, const char *text, int bgcol, int txtcol, int padleft, int padright );
/* gdImageFilledEllipseFast.c */
void gdImageFilledEllipseFast( gdImagePtr im, int mx, int my,
                               int w, int h, int c );
/* gdImageLoadFile */
gdImagePtr gdImageLoadFile( const char *filename );

#endif

