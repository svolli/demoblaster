
#include "gd_db.h"

/* this is taken from gdImageCopyMerge and slightly edited */
void gdImageCopyAdd( gdImagePtr dst, gdImagePtr src, int dstX, int dstY,
                     int srcX, int srcY, int w, int h )
{
   int c, dc;
   int x, y;
   int tox, toy;
   int ncR, ncG, ncB;
   toy = dstY;
   for( y = srcY; y < (srcY + h); y++)
   {
      tox = dstX;
      for( x = srcX; x < (srcX + w); x++)
      {
         int nc;
         c = gdImageGetPixel( src, x, y);
         /* Added 7/24/95: support transparent copies */
         if( gdImageGetTransparent( src) == c)
         {
            tox++;
            continue;
         }
         /* If it's the same image, mapping is trivial */
         if( dst == src )
         {
            nc = c;
         }
         else
         {
            dc = gdImageGetPixel( dst, tox, toy );

            ncR = gdImageRed(src, c) + gdImageRed(dst, dc);
            ncG = gdImageGreen(src, c) + gdImageGreen(dst, dc);
            ncB = gdImageBlue(src, c) + gdImageBlue(dst, dc);

            /* Make color adding saturating */
            if( ncR > 0xff )
            {
               ncR = 0xff;
            }
            if( ncG > 0xff )
            {
               ncG = 0xff;
            }
            if( ncB > 0xff )
            {
               ncB = 0xff;
            }

            /* Find a reasonable color */
            nc = gdImageColorResolve( dst, ncR, ncG, ncB );
         }
         gdImageSetPixel( dst, tox, toy, nc);
         tox++;
      }
      toy++;
   }
}
