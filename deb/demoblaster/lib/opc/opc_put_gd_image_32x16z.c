
#include "opcgd.h"


#define PIXELS_PER_LINE (32)
#define PIXEL_LINES (16)
#define MAX_PIXELS (PIXELS_PER_LINE * PIXEL_LINES)
#define MAX_CHANNELS (1)

static pixel pixels[MAX_PIXELS];

#if 0
typedef void (*prepare_t)(void**);
typedef int  (*runframe_t)(void*); /* return value: call again in n frames */
typedef void (*cleanup_t)(void*);

typedef struct {
   prepare_t      prepare;
   runframe_t     runframe;
   cleanup_t      cleanup;
   unsigned int   flags;
} demopart_t;
#define DEMOPART_FLAG_EARLYPRECALC (1<<0)
#endif

const unsigned short tlm_32x16z[512] = {
   0,  1,  2,  3,  4,  5,  6,  7, 64, 65, 66, 67, 68, 69, 70, 71,128,129,130,131,132,133,134,135,192,193,194,195,196,197,198,199
, 15, 14, 13, 12, 11, 10,  9,  8, 79, 78, 77, 76, 75, 74, 73, 72,143,142,141,140,139,138,137,136,207,206,205,204,203,202,201,200
, 16, 17, 18, 19, 20, 21, 22, 23, 80, 81, 82, 83, 84, 85, 86, 87,144,145,146,147,148,149,150,151,208,209,210,211,212,213,214,215
, 31, 30, 29, 28, 27, 26, 25, 24, 95, 94, 93, 92, 91, 90, 89, 88,159,158,157,156,155,154,153,152,223,222,221,220,219,218,217,216
, 32, 33, 34, 35, 36, 37, 38, 39, 96, 97, 98, 99,100,101,102,103,160,161,162,163,164,165,166,167,224,225,226,227,228,229,230,231
, 47, 46, 45, 44, 43, 42, 41, 40,111,110,109,108,107,106,105,104,175,174,173,172,171,170,169,168,239,238,237,236,235,234,233,232
, 48, 49, 50, 51, 52, 53, 54, 55,112,113,114,115,116,117,118,119,176,177,178,179,180,181,182,183,240,241,242,243,244,245,246,247
, 63, 62, 61, 60, 59, 58, 57, 56,127,126,125,124,123,122,121,120,191,190,189,188,187,186,185,184,255,254,253,252,251,250,249,248
,256,257,258,259,260,261,262,263,320,321,322,323,324,325,326,327,384,385,386,387,388,389,390,391,448,449,450,451,452,453,454,455
,271,270,269,268,267,266,265,264,335,334,333,332,331,330,329,328,399,398,397,396,395,394,393,392,463,462,461,460,459,458,457,456
,272,273,274,275,276,277,278,279,336,337,338,339,340,341,342,343,400,401,402,403,404,405,406,407,464,465,466,467,468,469,470,471
,287,286,285,284,283,282,281,280,351,350,349,348,347,346,345,344,415,414,413,412,411,410,409,408,479,478,477,476,475,474,473,472
,288,289,290,291,292,293,294,295,352,353,354,355,356,357,358,359,416,417,418,419,420,421,422,423,480,481,482,483,484,485,486,487
,303,302,301,300,299,298,297,296,367,366,365,364,363,362,361,360,431,430,429,428,427,426,425,424,495,494,493,492,491,490,489,488
,304,305,306,307,308,309,310,311,368,369,370,371,372,373,374,375,432,433,434,435,436,437,438,439,496,497,498,499,500,501,502,503
,319,318,317,316,315,314,313,312,383,382,381,380,379,378,377,376,447,446,445,444,443,442,441,440,511,510,509,508,507,506,505,504
};

u8 opc_put_gd_image_32x16z( opc_sink s, gdImagePtr im )
{
   if( (im->sx == PIXELS_PER_LINE) && (im->sy == PIXEL_LINES) )
   {
      /* image matches hardware dimensions: send */
      int p;
      int gdpixel;
      pixel *opcpixel;
      if( im->trueColor )
      {
         for( p = 0; p < MAX_PIXELS; ++p )
         {
            gdpixel     = gdImageGetPixel( im, p % PIXELS_PER_LINE, p / PIXELS_PER_LINE );
            opcpixel    = &(pixels[tlm_32x16z[p]]);
            opcpixel->r = gdTrueColorGetRed( gdpixel );
            opcpixel->g = gdTrueColorGetGreen( gdpixel );
            opcpixel->b = gdTrueColorGetBlue( gdpixel );
         }
      }
      else
      {
         for( p = 0; p < MAX_PIXELS; ++p )
         {
            gdpixel     = gdImageGetPixel( im, p % PIXELS_PER_LINE, p / PIXELS_PER_LINE );
            opcpixel    = &(pixels[tlm_32x16z[p]]);
            opcpixel->r = im->red[gdpixel];
            opcpixel->g = im->green[gdpixel];
            opcpixel->b = im->blue[gdpixel];
         }
      }
      return opc_put_pixels( s, 0, MAX_PIXELS, pixels );
   }
   else
   {
      u8 retval;
      /* image does not match hardware dimensions: rescale */
      gdImage *newImg = gdImageCreateTrueColor( PIXELS_PER_LINE, PIXEL_LINES );
      gdImageCopyResampled( newImg, (gdImagePtr)im, 0, 0, 0, 0, newImg->sx, newImg->sy, im->sx, im->sy );
      retval = opc_put_gd_image_32x16z( s, newImg );
      gdImageDestroy( newImg );
      return retval;
   }
}
