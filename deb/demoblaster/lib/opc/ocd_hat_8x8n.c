
#include "opcgd.h"

static const char ocd_hat_8x8n_hostport[] = "localhost:7891";

static const unsigned short ocd_hat_8x8n_tlm[] =
{ 0, 1, 2, 3, 4, 5, 6, 7,
  8, 9,10,11,12,13,14,15,
 16,17,18,19,20,21,22,23,
 24,25,26,27,28,29,30,31,
 32,33,34,35,36,37,38,39,
 40,41,42,43,44,45,46,47,
 48,49,50,51,52,53,54,55,
 56,57,58,59,60,61,62,63 };

static pixel ocd_hat_8x8n_pixels[64];

struct opc_client_description_s ocd_hat_8x8n_data =
{
   8,
   8,
   &ocd_hat_8x8n_tlm[0],
   &ocd_hat_8x8n_pixels[0],
   &ocd_hat_8x8n_hostport[0]
};

opc_client_description_t ocd_hat_8x8n = &ocd_hat_8x8n_data;
