
#include "opcgd.h"
#include <assert.h>

opc_client_description_t ocds[OPC_MAX_SINKS] = { 0 };

opc_sink opc_new_gd_sink( opc_client_description_t ocd )
{
   opc_sink s = opc_new_sink( (char*)ocd->hostnameport );
   if( (s >= 0) && (s < OPC_MAX_SINKS) )
   {
      ocds[s] = ocd;
   }
   return s;
}

gdImagePtr opc_create_gd_image( opc_sink s )
{
   assert( (s >= 0) && (s < OPC_MAX_SINKS) );
   return gdImageCreateTrueColor( ocds[s]->x, ocds[s]->y );
}

u8 opc_put_gd_image( opc_sink s, gdImagePtr im )
{
   int pixels_per_line        = ocds[s]->x;
   int pixel_lines            = ocds[s]->y;
   int max_pixels             = pixels_per_line * pixel_lines;
   const unsigned short *tlm  = ocds[s]->tlm;
   pixel *pixels              = ocds[s]->pixels;

   assert( (s >= 0) && (s < OPC_MAX_SINKS) );
   if( (im->sx == pixels_per_line) && (im->sy == pixel_lines) )
   {
      /* image matches hardware dimensions: send */
      int p;
      int gdpixel;
      pixel *opcpixel;
      if( im->trueColor )
      {
         if( tlm )
         {
            for( p = 0; p < max_pixels; ++p )
            {
               gdpixel     = gdImageGetPixel( im, p % pixels_per_line, p / pixels_per_line );
               opcpixel    = &(pixels[tlm[p]]);
               opcpixel->r = gdTrueColorGetRed( gdpixel );
               opcpixel->g = gdTrueColorGetGreen( gdpixel );
               opcpixel->b = gdTrueColorGetBlue( gdpixel );
            }
         }
         else
         {
            for( p = 0; p < max_pixels; ++p )
            {
               gdpixel     = gdImageGetPixel( im, p % pixels_per_line, p / pixels_per_line );
               opcpixel    = &(pixels[p]);
               opcpixel->r = gdTrueColorGetRed( gdpixel );
               opcpixel->g = gdTrueColorGetGreen( gdpixel );
               opcpixel->b = gdTrueColorGetBlue( gdpixel );
            }
         }
      }
      else
      {
         if( tlm )
         {
            for( p = 0; p < max_pixels; ++p )
            {
               gdpixel     = gdImageGetPixel( im, p % pixels_per_line, p / pixels_per_line );
               opcpixel    = &(pixels[tlm[p]]);
               opcpixel->r = im->red[gdpixel];
               opcpixel->g = im->green[gdpixel];
               opcpixel->b = im->blue[gdpixel];
            }
         }
         else
         {
            for( p = 0; p < max_pixels; ++p )
            {
               gdpixel     = gdImageGetPixel( im, p % pixels_per_line, p / pixels_per_line );
               opcpixel    = &(pixels[p]);
               opcpixel->r = im->red[gdpixel];
               opcpixel->g = im->green[gdpixel];
               opcpixel->b = im->blue[gdpixel];
            }
         }
      }
      return opc_put_pixels( s, 0, max_pixels, pixels );
   }
   else
   {
      u8 retval;
      /* image does not match hardware dimensions: rescale */
      gdImage *newImg = gdImageCreateTrueColor( pixels_per_line, pixel_lines );
      gdImageCopyResampled( newImg, im, 0, 0, 0, 0, newImg->sx, newImg->sy, im->sx, im->sy );
      retval = opc_put_gd_image( s, newImg );
      gdImageDestroy( newImg );
      return retval;
   }
}
