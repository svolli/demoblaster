
#include "opcgd.h"

static const char ocd_hat_8x8w_hostport[] = "localhost:7891";

static const unsigned short ocd_hat_8x8w_tlm[] =
{ 7,15,23,31,39,47,55,63,
  6,14,22,30,38,46,54,62,
  5,13,21,29,37,45,53,61,
  4,12,20,28,36,44,52,60,
  3,11,19,27,35,43,51,59,
  2,10,18,26,34,42,50,58,
  1, 9,17,25,33,41,49,57,
  0, 8,16,24,32,40,48,56 };

static pixel ocd_hat_8x8w_pixels[64];

struct opc_client_description_s ocd_hat_8x8w_data =
{
   8,
   8,
   &ocd_hat_8x8w_tlm[0],
   &ocd_hat_8x8w_pixels[0],
   &ocd_hat_8x8w_hostport[0]
};

opc_client_description_t ocd_hat_8x8w = &ocd_hat_8x8w_data;
