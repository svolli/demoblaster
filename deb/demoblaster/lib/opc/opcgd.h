
#ifndef LIBOPC_OPCGD_H
#define LIBOPC_OPCGD_H LIBOPC_OPCGD_H

#include <opc.h>
#include <gd.h>
#include <cJSON.h>

/* ocd (opc client description) specification */
struct opc_client_description_s
{
   const unsigned short x;             /* width */
   const unsigned short y;             /* height */
   const unsigned short *tlm;          /* translation matrix */
   pixel                *pixels;       /* databuffer for sending */
   const char           *hostnameport; /* format: "hostname:port" */
};

typedef struct opc_client_description_s *opc_client_description_t;

/* ocd_*x*.c */
/* predefined: fadecandy 32x16z */
extern opc_client_description_t ocd_fc_32x16z;
/* predefined: sense hat top led row facing north */
extern opc_client_description_t ocd_hat_8x8n;
/* predefined: sense hat top led row facing west */
extern opc_client_description_t ocd_hat_8x8w;

/* ocd_read_json.c */
/* generic approach: read ocd from file */
opc_client_description_t ocd_from_json( const char *filename );
/* even more generic approach: read ocd from system wide installed file */
opc_client_description_t ocd_from_default();
/* clean up ocd created with ocd_from_json, do not use on predefined ocds */
void ocd_done( opc_client_description_t ocd );
/* helper function: read json file into cJSON struct */
cJSON *read_json( const char *filename );

/* opcgd.c */
/* create a new sink according to ocd */
opc_sink opc_new_gd_sink( opc_client_description_t ocd );
/* create a new true color image for the opc_sink, dimensions are taken from
   ocd passed to opc_new_gd_sink */
gdImagePtr opc_create_gd_image( opc_sink s );
/* send image to sink, using ocd passwd to opc_new_gd_sink */
u8 opc_put_gd_image( opc_sink s, gdImagePtr im );

/* opc_put_gd_image_*.c */
/* send an image to fadecandy using sink created with opc_new_sink */
u8 opc_put_gd_image_32x16z( opc_sink s, gdImagePtr im );
/* send an image to sense hat using sink created with opc_new_sink */
u8 opc_put_gd_image_8x8l( opc_sink s, gdImagePtr im );

#endif

