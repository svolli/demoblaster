
#include "opcgd.h"


#define PIXELS_PER_LINE (8)
#define PIXEL_LINES (8)
#define MAX_PIXELS (PIXELS_PER_LINE * PIXEL_LINES)
#define MAX_CHANNELS (1)

static pixel pixels[MAX_PIXELS];

u8 opc_put_gd_image_8x8l( opc_sink s, gdImagePtr im )
{
   if( (im->sx == PIXELS_PER_LINE) && (im->sy == PIXEL_LINES) )
   {
      /* image matches hardware dimensions: send */
      int p;
      int gdpixel;
      pixel *opcpixel;
      if( im->trueColor )
      {
         for( p = 0; p < MAX_PIXELS; ++p )
         {
            gdpixel     = gdImageGetPixel( im, p % PIXELS_PER_LINE, p / PIXELS_PER_LINE );
            opcpixel    = &(pixels[p]);
            opcpixel->r = gdTrueColorGetRed( gdpixel );
            opcpixel->g = gdTrueColorGetGreen( gdpixel );
            opcpixel->b = gdTrueColorGetBlue( gdpixel );
         }
      }
      else
      {
         for( p = 0; p < MAX_PIXELS; ++p )
         {
            gdpixel     = gdImageGetPixel( im, p % PIXELS_PER_LINE, p / PIXELS_PER_LINE );
            opcpixel    = &(pixels[p]);
            opcpixel->r = im->red[gdpixel];
            opcpixel->g = im->green[gdpixel];
            opcpixel->b = im->blue[gdpixel];
         }
      }
      return opc_put_pixels( s, 0, MAX_PIXELS, pixels );
   }
   else
   {
      u8 retval;
      /* image does not match hardware dimensions: rescale */
      gdImage *newImg = gdImageCreateTrueColor( PIXELS_PER_LINE, PIXEL_LINES );
      gdImageCopyResampled( newImg, (gdImagePtr)im, 0, 0, 0, 0, newImg->sx, newImg->sy, im->sx, im->sy );
      retval = opc_put_gd_image_8x8l( s, newImg );
      gdImageDestroy( newImg );
      return retval;
   }
}
