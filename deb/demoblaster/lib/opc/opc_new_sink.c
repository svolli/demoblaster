
#include <opc.h>

opc_sink opc_new_sink(char* hostport)
{
   if( !hostport )
   {
      hostport = "127.0.0.1:7890";
   }
   return opc_new_sink_socket(hostport);
}                                                                                                         

