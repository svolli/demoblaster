
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "opcgd.h"

#define SYSTEM_DEFAULT_JSON "/etc/fadecandy/layout.json"


void ocd_done( opc_client_description_t d )
{
   if( d->hostnameport )
   {
      free( (void*)d->hostnameport );
   }
   if( d->pixels )
   {
      free( d->pixels );
   }
   if( d->tlm )
   {
      free( (void*)d->tlm );
   }
   free( d );
}


cJSON *read_json( const char *filename )
{
   cJSON *retval = 0;
   int fd;
   struct stat sb;
   char *buffer;
   int readpos;
   int chunksize;

   fd = open( filename, O_RDONLY );
   if( fd < 0 )
   {
      perror( filename );
      return 0;
   }
   if( fstat( fd, &sb ) < 0 )
   {
      perror( filename );
      close( fd );
      return 0;
   }

   buffer = (char*)malloc( sb.st_size + 1 );
   buffer[sb.st_size] = '\0';
   for( readpos = 0; readpos < sb.st_size; readpos += chunksize )
   {
      chunksize = read( fd, buffer + readpos, sb.st_size - readpos );
      if( chunksize <= 0 )
      {
         perror( filename );
         close( fd );
         free( buffer );
         return 0;
      }
   }
   close( fd );

   retval = cJSON_Parse( buffer );

   free( buffer );

   return retval;
}


opc_client_description_t ocd_from_json( const char *filename )
{
   opc_client_description_t retval = 0;
   cJSON *root;
   cJSON *connect;
   cJSON *x;
   cJSON *y;
   cJSON *translationmatrix;

   int i;
   int errors = 0;

   root = read_json( filename );
   if( !root )
   {
      fprintf( stderr, "parse error before: [%s]\n",cJSON_GetErrorPtr() );
      return 0;
   }

   connect           = cJSON_GetObjectItem( root, "connect" );
   x                 = cJSON_GetObjectItem( root, "x" );
   y                 = cJSON_GetObjectItem( root, "y" );
   translationmatrix = cJSON_GetObjectItem( root, "translationmatrix" );

   if( !connect )
   {
      fprintf( stderr, "'connect' not set\n" );
      ++errors;
   }
   else
   {
      if( connect->type != cJSON_String )
      {
         fprintf( stderr, "'connect' should be string\n" );
         ++errors;
      }
   }

   if( !x )
   {
      x = cJSON_GetObjectItem( root, "width" );
   }
   if( !x )
   {
      fprintf( stderr, "'x'/'width' not set\n" );
      ++errors;
   }
   else
   {
      if( x->type != cJSON_Number )
      {
         fprintf( stderr, "'x'/'width' should be integer\n" );
         ++errors;
      }
   }

   if( !y )
   {
      y = cJSON_GetObjectItem( root, "height" );
   }
   if( !y )
   {
      fprintf( stderr, "'y'/'height' not set\n" );
      ++errors;
   }
   else
   {
      if( y->type != cJSON_Number )
      {
         fprintf( stderr, "'y'/'height' should be integer\n" );
         ++errors;
      }
   }

   if( !translationmatrix )
   {
      /*fprintf( stderr, "'translationmatrix' not set\n" );*/
   }
   else
   {
      if( translationmatrix->type != cJSON_Array )
      {
         fprintf( stderr, "'translationmatrix' should be array\n" );
         ++errors;
      }
      else
      {
         if( cJSON_GetArraySize( translationmatrix ) != (x->valueint * y->valueint) )
         {
            fprintf( stderr, "'translationmatrix' contains %d elements instead of %d\n",
                     cJSON_GetArraySize( translationmatrix ), (x->valueint * y->valueint) );
            ++errors;
         }
         for( i = 0; i < cJSON_GetArraySize( translationmatrix ); ++i )
         {
            cJSON *element = cJSON_GetArrayItem( translationmatrix, i );
            if( element->type != cJSON_Number )
            {
               fprintf( stderr, "entry %d in translationmatrix is not a number\n", i );
               ++errors;
            }
         }
      }
   }

   if( errors > 0 )
   {
      fprintf( stderr, "json file contains at least %d errors\n", errors );
   }
   else
   {
      unsigned short *checkmatrix = 0;
      unsigned short tmp_x, tmp_y;
      retval = (opc_client_description_t)malloc( sizeof(*retval) );
      retval->hostnameport = strdup( connect->valuestring );
      tmp_x = x->valueint;
      tmp_y = y->valueint;
      memcpy( (void*)&(retval->x), &(tmp_x), sizeof(retval->x) );
      memcpy( (void*)&(retval->y), &(tmp_y), sizeof(retval->y) );
      retval->pixels = (pixel*)malloc( sizeof(pixel) * retval->x * retval->y );
      retval->tlm = 0;
      if( translationmatrix )
      {
         unsigned short *tlm = (unsigned short*)malloc( sizeof( unsigned short ) * retval->x * retval->y );
         retval->tlm = (const unsigned short*)tlm;
         checkmatrix = (unsigned short*)calloc( 1, sizeof( unsigned short ) * retval->x * retval->y );
         for( i = 0; i < cJSON_GetArraySize( translationmatrix ); ++i )
         {
            tlm[i] = cJSON_GetArrayItem( translationmatrix, i )->valueint;
            checkmatrix[tlm[i]]++;
         }
         for( i = 0; i < (retval->x * retval->y); ++i )
         {
            if( checkmatrix[i] < 1 )
            {
               fprintf( stderr, "translationmatrix: '%d' is missing\n", i );
               ++errors;
            }
            else if( checkmatrix[i] > 1 )
            {
               fprintf( stderr, "translationmatrix: '%d' is appearing %d times\n", i, checkmatrix[i] );
               ++errors;
            }
         }
         free( checkmatrix ); checkmatrix = 0;
         if( errors > 0 )
         {
            fprintf( stderr, "json file contains at least %d errors\n", errors );
            ocd_done( retval ); retval = 0;
         }
      }
      free( retval->hostnameport );
   }

   cJSON_Delete( root );

   return retval;
}


opc_client_description_t ocd_from_default()
{
   return ocd_from_json( SYSTEM_DEFAULT_JSON );
}
