
#include <math.h>
#include <stdlib.h>

#include "effects.h"

struct twister_s
{
   gdImagePtr  im;
   gdImagePtr  work;
   int         horizontal;
   float       start;
   float       step;
   float       twist;
   hsv_t       color[2];
};


twister_t twister_init( gdImagePtr im, int oversampling )
{
   twister_t d = (twister_t)malloc( sizeof( struct twister_s ) );
   d->im             = im;
   d->work           = gdImageCreateTrueColor( im->sx * oversampling, im->sy * oversampling );
   d->horizontal     = 0;
   d->start          = 0.0;
   d->step           = 0.1;
   d->twist          = 0.0;
   d->color[0].h     = 0.0;
   d->color[0].s     = 1.0;
   d->color[0].v     = 1.0;
   d->color[1].h     = 1.0 / 3;
   d->color[1].s     = 1.0;
   d->color[1].v     = 1.0;

   /* set black as transparent color */
   gdImageColorTransparent( d->work, 0x000000 );

   return d;
}


void twister_done( twister_t d )
{
   gdImageDestroy( d->work );
   free( d );
}


void twister_set_colors( twister_t d, hsv_t *color0, hsv_t *color1 )
{
   d->color[0] = *color0;
   d->color[1] = *color1;
}


void twister_set_twist( twister_t d, int horizontal, float twist )
{
   d->horizontal = horizontal;
   d->twist      = twist;
}


void twister_calc( twister_t d )
{
   float point[4][2];
   float la = 0.0;
   int x, sizex;
   int y, sizey;
   hsv_t color;
   hsv_t color0;
   hsv_t color1;
   float c0bas,c1bas,c0step,c1step;

   if( d->horizontal )
   {
      sizex = d->work->sy;
      sizey = d->work->sx;
   }
   else
   {
      sizex = d->work->sx;
      sizey = d->work->sy;
   }

   for( y = 0; y < sizey; ++y )
   {
      int x0,x1,x2;
      for( x = 0; x < 4; ++x )
      {
         point[x][0] = (sinf( la + d->start + M_PI_2 * x + M_PI - M_PI_4 ) + 1.0) / 2;
         point[x][1] = (cosf( la + d->start + M_PI_2 * x + M_PI - M_PI_4 ) + 1.0) / 2;
      }
      if( point[0][0] >= point[1][0] )
      {
         color0 = d->color[0];
         color1 = d->color[1];
         x0 = (int)(point[1][0] * sizex);
         x1 = (int)(point[2][0] * sizex);
         x2 = (int)(point[3][0] * sizex);
         c0bas = point[1][1];
         c1bas = point[2][1];
         c0step = (point[2][1]-point[1][1]) / (x1-x0);
         c1step = (point[3][1]-point[2][1]) / (x2-x1);
      }
      else
      {
         color0 = d->color[1];
         color1 = d->color[0];
         x0 = (int)(point[0][0] * sizex);
         x1 = (int)(point[1][0] * sizex);
         x2 = (int)(point[2][0] * sizex);
         c0bas = point[0][1];
         c1bas = point[1][1];
         c0step = (point[1][1]-point[0][1]) / (x1-x0);
         c1step = (point[2][1]-point[1][1]) / (x2-x1);
      }

      for( x = 0; x < sizex; ++x )
      {
         if( x < x0 )
         {
            color.h = 0.0;
            color.s = 0.0;
            color.v = 0.0;
         }
         else if( x < x1 )
         {
            color = color0;
            color.v *= (c0bas + c0step * (x-x0));
         }
         else if( x < x2 )
         {
            color = color1;
            color.v *= (c1bas + c1step * (x-x1));
         }
         else
         {
            color.h = 0.0;
            color.s = 0.0;
            color.v = 0.0;
         }

         if( d->horizontal )
         {
            gdImageSetPixel( d->work, y, x, hsv_to_rgb( &color ) );
         }
         else
         {
            gdImageSetPixel( d->work, x, y, hsv_to_rgb( &color ) );
         }
      }

      la += d->twist / (d->work->sx / d->im->sx);
      while( (la + d->start) >= M_PI )
      {
         la -= M_PI;
      }
      while( (la + d->start) < 0 )
      {
         la += M_PI;
      }
   }

   d->start += d->step;
   if( d->start >= M_PI )
   {
      d->start -= M_PI;
   }

   gdImageCopyResampled( d->im, d->work, 0, 0, 0, 0, d->im->sx, d->im->sy, d->work->sx, d->work->sy );
}
