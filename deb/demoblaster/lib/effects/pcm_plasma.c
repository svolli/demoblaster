
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "effects.h"

struct pcm_plasma_s
{
   gdImagePtr         im;
   gdImagePtr         work;
   int                oversampling;
   float              time;
   float              increment;
   const unsigned int *palette;
   int                wave[4];
   unsigned char      sintable[1024];
};


pcm_plasma_t pcm_plasma_init( gdImagePtr im, int oversampling, int start_time )
{
   int i;
   // pre-calc sinus table

   pcm_plasma_t d = (pcm_plasma_t)malloc( sizeof( struct pcm_plasma_s ) );

   d->time = start_time;
   d->time /= 10000000;
   d->increment = 0.03;
   d->oversampling = oversampling;
   d->im = im;

   for( i = 0; i < sizeof(d->sintable)/sizeof(d->sintable[0]); i++ )
   {
      d->sintable[i] = 128+127*sin((float)i*M_PI/512.0);
   }

   for( i = 0; i < sizeof(d->wave)/sizeof(d->wave[0]); i++)
   {
      d->wave[i] = 0;//(rand() % 2048) - 1024;
   }

   d->work = gdImageCreateTrueColor( im->sx * oversampling, im->sy * oversampling );

   return d;
}


void pcm_plasma_done( pcm_plasma_t d )
{
   gdImageDestroy( d->work );
   memset( d, 0, sizeof(*d) );
   free( d );
}


void pcm_plasma_set_palette( pcm_plasma_t d, const unsigned int *palette )
{
   d->palette = palette;
}


void pcm_plasma_calc( pcm_plasma_t d )
{
   int y, x;
   int speed_x[4];
   int speed_y[4];
   int workwave_x[4];
   int workwave_y[4];
   register int i;

   // Speed-Werte der Wellen vorberechnen
   speed_x[0] = 30000 * sin ((d->time)*1.0+1)  / d->oversampling;
   speed_y[0] = 30000 * cos ((d->time)*1.1+2)  / d->oversampling;
   speed_x[1] =  7000 * sin (-(d->time)*1.2+3) / d->oversampling;
   speed_y[1] =  7000 * cos (-(d->time)*1.3+4) / d->oversampling;
   speed_x[2] = 23000 * sin ((d->time)*1.4+5)  / d->oversampling;
   speed_y[2] = 23000 * cos ((d->time)*1.5+6)  / d->oversampling;
   speed_x[3] = 17000 * sin ((d->time)*1.6+7)  / d->oversampling;
   speed_y[3] = 17000 * cos ((d->time)*1.7+8)  / d->oversampling;

   for ( i=0; i<4; i++)
   {
      d->wave[i] += speed_x[i];
      workwave_y[i]      = d->wave[i];
   }

   // Schleife ueber die Hoehe des Bitmaps
   for( y = 0; y < d->work->sy; ++y )
   {
      for( i = 0; i < 4; ++i )
      {
         workwave_x[i] = workwave_y[i];
      }

      // Scanline zeichnen
      for( x = 0; x < d->work->sx; ++x )
      {
         int colidx = (d->sintable[(workwave_x[0]>>8)&1023] +
                       d->sintable[(workwave_x[1]>>8)&1023] +
                       d->sintable[(workwave_x[2]>>8)&1023] +
                       d->sintable[(workwave_x[3]>>8)&1023]);
         gdImageSetPixel( d->work, x, y, d->palette[colidx] );

         for( i = 0; i < 4; ++i )
         {
            workwave_x[i] += speed_x[i];
         }
      }
      for( i = 0; i < 4; ++i )
      {
         workwave_y[i] += speed_y[i];
      }
   }
   d->time += d->increment;
   gdImageCopyResampled( d->im, d->work, 0, 0, 0, 0, d->im->sx, d->im->sy, d->work->sx, d->work->sy );
}
