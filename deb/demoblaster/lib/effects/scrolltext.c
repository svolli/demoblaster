
#include <stdlib.h>

#include "effects.h"
#include "../gd_db/gd_db.h"

struct scrolltext_s
{
   gdImagePtr  im;
   gdImagePtr  work;
   int         x;
   int         y;
   int         xsize;
   int         ysize;
   int         xpos;
};


scrolltext_t scrolltext_init( gdImagePtr im, gdFontPtr font,
                              int x, int y, int width, int height,
                              const char *text, int bgcol, int txtcol )
{
   scrolltext_t d = (scrolltext_t)malloc( sizeof( *d ) );
   d->im          = im;
   d->work        = gdImageCreateFromText( font, text, bgcol, txtcol, width, width + font->w );
   d->x           = x;
   d->y           = y;
   d->xsize       = width;
   d->ysize       = height;
   d->xpos        = 0;

   return d;
}


void scrolltext_done( scrolltext_t d )
{
   gdImageDestroy( d->work );
   free( d );
}


int scrolltext_calc( scrolltext_t d )
{
   gdImageCopy( d->im, d->work, d->x, d->y, d->xpos, 0, d->xsize, d->ysize );

   if( ++d->xpos > (d->work->sx - d->xsize) )
   {
      d->xpos = 0;
   }
   return d->xpos;
}
