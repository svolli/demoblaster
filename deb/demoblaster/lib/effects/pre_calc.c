
#define _GNU_SOURCE
#include <pthread.h>

#include <errno.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>

#include "effects.h"


struct pre_calc_s
{
   pthread_t      thread_id;
};


pre_calc_t pre_calc_init()
{
   pre_calc_t d = (pre_calc_t)malloc( sizeof(struct pre_calc_s) );

   memset( d, 0, sizeof(struct pre_calc_s) );

   return d;
}


void *pre_calc_done( pre_calc_t d )
{
   int err;
   void *retval;
   wait_until_t w = wait_until_init();
   wait_until_ms( w, 2 );
   err = pthread_join( d->thread_id, &retval );
   if( err )
   {
      perror( "pthread_attr_init" );
      exit( EXIT_FAILURE );
   }

   err = wait_until_timeout( w );
   if( err == 1 )
   {
      puts( "^^ pre_calc ^^" );
   }
   wait_until_done( w );

   free( d );
   return retval;
}


void pre_calc_start( pre_calc_t d, void *(*start_routine) (void *), void *arg )
{
   int err;
   pthread_attr_t attr;

   err = pthread_attr_init( &attr );
   if( err )
   {
      perror( "pthread_attr_init" );
      exit( EXIT_FAILURE );
   }

   err = pthread_create( &(d->thread_id), &attr, start_routine, arg );
   if( err )
   {
      perror( "pthread_attr_init" );
      exit( EXIT_FAILURE );
   }

   err = pthread_attr_destroy( &attr );
   if( err )
   {
      perror( "pthread_attr_destroy" );
      exit( EXIT_FAILURE );
   }
}
