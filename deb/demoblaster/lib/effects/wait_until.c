
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <time.h>

#include "effects.h"


struct wait_until_s
{
   struct timespec   then;
   long              ms;
};


wait_until_t wait_until_init()
{
   wait_until_t d = (wait_until_t)malloc( sizeof(struct wait_until_s) );

   memset( d, 0, sizeof(struct wait_until_s) );

   return d;
}


void wait_until_done( wait_until_t d )
{
   free( d );
}


static inline int wait_until_set_now( wait_until_t d )
{
   return clock_gettime( CLOCK_MONOTONIC, &(d->then) );
}


static inline void wait_until_add_ms( wait_until_t d )
{
   d->then.tv_nsec += (d->ms * 1000000);
   d->then.tv_sec  += (d->ms / 1000);
   if( d->then.tv_nsec > 1000000000 )
   {
      d->then.tv_nsec -= 1000000000;
      d->then.tv_sec  += 1;
   }
}

int wait_until_ms( wait_until_t d, long ms )
{
   int retval;
   d->ms = ms;
   retval = wait_until_set_now( d );
   if( retval )
   {
      return retval;
   }
   wait_until_add_ms( d );
   return retval;
}


int wait_until_timeout( wait_until_t d )
{
   int retval;
   struct timespec now;
   int delay;

   if( d->ms <= 0 )
   {
      return 0;
   }

   retval = clock_gettime( CLOCK_MONOTONIC, &now );
   if( retval )
   {
      return retval;
   }

   wait_until_add_ms( d );

   /* delay contains ms */
   delay  = (d->then.tv_sec  - now.tv_sec)  * 1000;
   delay += (d->then.tv_nsec - now.tv_nsec) / 1000000;

   if( delay <= 0 )
   {
      printf( "%d ms too late\n", -delay );
      wait_until_set_now( d );
      wait_until_add_ms( d );
      return 1;
   }
   return usleep( (useconds_t)(delay * 1000) );
}
