
#include <endian.h>
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>

#include "effects.h"

#define SCALE   (2)

struct image_zoom_s
{
   gdImagePtr  im;
   int         listsize;
   gdImagePtr  *list;
   int         *scale;
};


image_zoom_t image_zoom_init( gdImagePtr im, gdImagePtr src )
{
   int i;
   int sizex, sizey;
   image_zoom_t d = (image_zoom_t)malloc( sizeof( struct image_zoom_s ) );
   d->im          = im;

   if( (src->sx / d->im->sx) >= (src->sy / d->im->sy) )
   {
      /* use X as reference */
      sizex = src->sx;
      for( d->listsize = 0; (sizex / SCALE) > d->im->sx; ++(d->listsize) )
      {
         sizex /= SCALE;
      }
   }
   else
   {
      /* use Y as reference */
      sizey = src->sy;
      for( d->listsize = 0; (sizey / SCALE) > d->im->sy; ++(d->listsize) )
      {
         sizey /= SCALE;
      }
   }
   if( !(d->listsize) )
   {
      d->listsize = 1;
   }

   d->list  = (gdImagePtr*)malloc( sizeof(gdImagePtr) * d->listsize );
   d->scale = (int*)malloc( sizeof(int) * d->listsize );
   d->list[0] = gdImageCreateTrueColor( src->sx, src->sy );
   d->scale[0] = 1;
   gdImageCopy( d->list[0], src, 0, 0, 0, 0, src->sx, src->sy );
   sizex = src->sx;
   sizey = src->sy;
   for( i = 1; i < d->listsize ;++i )
   {
      sizex = d->list[i-1]->sx;
      sizey = d->list[i-1]->sy;
      d->list[i] = gdImageCreateTrueColor( sizex/SCALE, sizey/SCALE );
      d->scale[i] = d->list[0]->sx / d->list[i]->sx;
      gdImageCopyResampled( d->list[i], d->list[i-1], 0, 0, 0, 0, sizex/SCALE, sizey/SCALE, sizex, sizey );
   }

   return d;
}


void image_zoom_done( image_zoom_t d )
{
   int i = 0;
   for( i = 0; i < d->listsize; ++i )
   {
      gdImageDestroy( d->list[i] );
   }
   free( d->list );
   free( d->scale );
   free( d );
}


void image_zoom_calc( image_zoom_t d, int cx, int cy, float zoom )
{
   int idx = 0;
   int i = 1;
   int sizex  = (int)(zoom * d->im->sx);
   int sizey  = (int)(zoom * d->im->sy);
   int startx = cx - sizex/2;
   int starty = cy - sizey/2;

   for( i = 0; i < d->listsize; ++i )
   {
      if( zoom >= d->scale[i] * d->scale[i] / 2 )
      {
         idx = i;
      }
      else
      {
         break;
      }
   }
   sizex  /= d->scale[idx];
   sizey  /= d->scale[idx];
   startx /= d->scale[idx];
   starty /= d->scale[idx];

   gdImageCopyResampled( d->im, d->list[idx], 0, 0, startx, starty, d->im->sx, d->im->sy, sizex, sizey );
}
