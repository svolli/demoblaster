
#include <stdlib.h>

#include "effects.h"
#include "../gd_db/gd_db.h"

struct ellipses_s
{
   gdImagePtr  im;
   gdImagePtr  work;
   int         color1;
   int         color2;
   int         x;
   int         y;
   int         xsize;
   int         ysize;
};


ellipses_t ellipses_init( gdImagePtr im, int oversampling )
{
   ellipses_t d = (ellipses_t)malloc( sizeof( struct ellipses_s ) );
   d->im          = im;
   d->work        = gdImageCreateTrueColor( im->sx * oversampling, im->sy * oversampling );
   d->color1      = 0xffffff;
   d->color2      = 0x000000;
   d->x           = d->work->sx / 2;
   d->y           = d->work->sy / 2;

   /* set black as transparent color */
   gdImageColorTransparent( d->work, 0x000000 );

   return d;
}


void ellipses_done( ellipses_t d )
{
   gdImageDestroy( d->work );
   free( d );
}


void ellipses_setpos( ellipses_t d, int x, int y )
{
   d->x = x;
   d->y = y;
}


void ellipses_setsize( ellipses_t d, int x, int y )
{
   d->xsize = x;
   d->ysize = y;
}


void ellipses_setcolor( ellipses_t d, int color1, int color2 )
{
   d->color1 = color1;
   d->color2 = color2;
}


void ellipses_set_calc( ellipses_t d, int x, int y, int xsize, int ysize, int color1, int color2 )
{
   d->x = x;
   d->y = y;
   d->xsize = xsize;
   d->ysize = ysize;
   d->color1 = color1;
   d->color2 = color2;

   ellipses_calc( d );
}


void ellipses_calc( ellipses_t d )
{
   int startx = d->work->sx - d->x;
   int starty = d->work->sy - d->y;
   int steps;

   if( startx < d->work->sx / 2 )
   {
      startx = d->work->sx - startx;
   }
   if( starty < d->work->sy / 2 )
   {
      starty = d->work->sy - starty;
   }

   if( (startx * d->ysize) > (starty * d->xsize) )
   {
      steps  = 3 * startx / d->xsize;
   }
   else
   {
      steps  = 3 * starty / d->ysize;
   }
   gdImageFilledRectangle( d->work, 0, 0, d->work->sx - 1, d->work->sy - 1, d->color2 );

   while( steps > 0 )
   {
      gdImageFilledEllipseFast( d->work, d->x, d->y, d->xsize * steps, d->ysize * steps, (steps & 1) ? d->color1 : d->color2 );
      --steps;
   }

   gdImageCopyResampled( d->im, d->work, 0, 0, 0, 0, d->im->sx, d->im->sy, d->work->sx, d->work->sy );
}
