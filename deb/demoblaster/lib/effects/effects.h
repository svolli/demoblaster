
#ifndef EFFECTS_H
#define EFFECTS_H EFFECTS_H

#include <gd.h>

#include "../colors/hsv.h"
#include "../colors/palettes.h"

#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif

#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif

/* pre_calc.c */
typedef struct pre_calc_s *pre_calc_t;
extern pre_calc_t pre_calc_init();
extern void *pre_calc_done( pre_calc_t d );
extern void pre_calc_start( pre_calc_t d, void *(*start_routine) (void *), void *arg );

/* wait_until.c */
typedef struct wait_until_s *wait_until_t;
extern wait_until_t wait_until_init();
extern void wait_until_done( wait_until_t d );
extern int wait_until_ms( wait_until_t d, long ms );
extern int wait_until_timeout( wait_until_t d );

/* image_zoom.c */
typedef struct image_zoom_s *image_zoom_t;
extern image_zoom_t image_zoom_init( gdImagePtr im, gdImagePtr src );
extern void image_zoom_done( image_zoom_t d );
extern void image_zoom_calc( image_zoom_t d, int cx, int cy, float zoom );

/* ifft.c */
typedef struct ifft_s *ifft_t;
ifft_t ifft_init( int in_bits, int out_bits );
void ifft_done( ifft_t ifft );
extern int16_t *ifft_calc( ifft_t ifft, int16_t *in_data );

/* mandelbrot.c */
typedef double mandelbrot_float;
typedef struct mandelbrot_s *mandelbrot_t;
mandelbrot_t mandelbrot_init( gdImagePtr im, int oversampling, int palette_size );
void mandelbrot_done( mandelbrot_t d );
void mandelbrot_setparams( mandelbrot_t d, mandelbrot_float zoom,
                           mandelbrot_float x, mandelbrot_float y );
void mandelbrot_setpalette( mandelbrot_t d, const unsigned int *palette );
unsigned int mandelbrot_minpalette( mandelbrot_t d );
unsigned int mandelbrot_maxpalette( mandelbrot_t d );
void mandelbrot_calc( mandelbrot_t d );

/* make_gradient.c */
extern void make_gradient( unsigned int *dest, unsigned int num_elems, 
                           unsigned int const *palette );

/* pcm_plasma.c */
typedef struct pcm_plasma_s *pcm_plasma_t;
extern pcm_plasma_t pcm_plasma_init( gdImagePtr im, int oversampling, int time_start );
extern void pcm_plasma_done( pcm_plasma_t d );
/* palette must be array of 1024 elements which must be valid during calc */
extern void pcm_plasma_set_palette( pcm_plasma_t d, const unsigned int *palette );
extern void pcm_plasma_calc( pcm_plasma_t d );

/* drop_circle.c */
typedef struct drop_circle_s *drop_circle_t;
extern drop_circle_t drop_circle_init( gdImagePtr im, int oversampling, int frames );
extern void drop_circle_done( drop_circle_t d );
extern void drop_circle_add( drop_circle_t d, int x, int y );
extern void drop_circle_calc( drop_circle_t d );

/* ellipses.c */
typedef struct ellipses_s *ellipses_t;
ellipses_t ellipses_init( gdImagePtr im, int oversampling );
void ellipses_done( ellipses_t d );
void ellipses_setpos( ellipses_t d, int x, int y );
void ellipses_setsize( ellipses_t d, int x, int y );
void ellipses_setcolor( ellipses_t d, int color1, int color2 );
void ellipses_set_calc( ellipses_t d, int x, int y, int xsize, int ysize, int color1, int color2 );
void ellipses_calc( ellipses_t d );

/* twister.c */
typedef struct twister_s *twister_t;
twister_t twister_init( gdImagePtr im, int oversampling );
void twister_done( twister_t d );
void twister_set_colors( twister_t d, hsv_t *color0, hsv_t *color1 );
void twister_set_twist( twister_t d, int horizontal, float twist );
void twister_calc( twister_t d );

/* scrolltext.c */
typedef struct scrolltext_s *scrolltext_t;
scrolltext_t scrolltext_init( gdImagePtr im, gdFontPtr font,
                              int x, int y, int width, int height,
                              const char *text, int bgcol, int txtcol );
void scrolltext_done( scrolltext_t d );
int scrolltext_calc( scrolltext_t d );


#define FAIL() { fprintf( stderr, "FAIL! at %s:%d\n", __FILE__, __LINE__ ); exit(1); }

#endif

