
#include <malloc.h>
#include <stdlib.h>

#include "effects.h"

#define NUM_STATES   (10)

struct drop_circle_s
{
   gdImagePtr  im;
   gdImagePtr  work;
   int         oversampling;
   int         size;
   int         frames;
   struct
   {
      int         x;
      int         y;
      int         current;
   }           states[NUM_STATES];
};


drop_circle_t drop_circle_init( gdImagePtr im, int oversampling, int frames )
{
   int i;
   drop_circle_t d = (drop_circle_t)malloc( sizeof( struct drop_circle_s ) );
   d->im           = im;
   d->work         = gdImageCreateTrueColor( im->sx * oversampling, im->sy * oversampling );
   d->size         = -1;
   d->frames       = frames;
   d->oversampling = oversampling;
   for( i = 0; i < NUM_STATES; ++i )
   {
      d->states[i].x = -1;
      d->states[i].y = -1;
      d->states[i].current = frames;
   }
   /* set black as transparent color */
   gdImageColorTransparent( d->work, 0x000000 );

   return d;
}


void drop_circle_done( drop_circle_t d )
{
   gdImageDestroy( d->work );
   free( d );
}


void drop_circle_add( drop_circle_t d, int x, int y )
{
   int i;
   for( i = 1; i < NUM_STATES; ++i )
   {
      d->states[i-1] = d->states[i];
   }
   d->states[NUM_STATES-1].x = (x * d->oversampling) + (rand() % d->oversampling);
   d->states[NUM_STATES-1].y = (y * d->oversampling) + (rand() % d->oversampling);
   d->states[NUM_STATES-1].current = 0;
}


void drop_circle_calc( drop_circle_t d )
{
   int radius = 0;
   int color  = 0;
   int i;
   const int pixel_size   = min( (d->work->sx / d->im->sx), (d->work->sy / d->im->sy) );
   const int max_diameter = (d->work->sx + d->work->sy ) / 2;
   /*
    * minimal radius: 1 pixel of im: (drop_im->sx / im->sx, drop_im->sy / im->sy)
    * maximal radius: min(sx,sy) pixel of im
    * step: (maxrad-minrad) / frames
    */

   gdImageFilledRectangle( d->work, 0, 0, d->work->sx-1, d->work->sy-1, 0x000000 );

   for( i = 0; i < NUM_STATES; ++i )
   {
      if( (d->states[i].current <= d->frames) && (d->states[i].x >= 0) && (d->states[i].y >= 0) )
      {
         float ff;

         ff = (float)max_diameter * d->states[i].current / d->frames;
         radius = pixel_size + ff;

         ff = 255.0 * d->states[i].current / d->frames;
         color = 0xff - (int)ff;
         color = color << 16 | color << 8 | color;
         ++(d->states[i].current);
      }

      if( radius > 0 )
      {
         gdImageFilledEllipse( d->work, d->states[i].x, d->states[i].y, radius, radius, color );
      }
   }
   gdImageCopyResampled( d->im, d->work, 0, 0, 0, 0, d->im->sx, d->im->sy, d->work->sx, d->work->sy );
}
