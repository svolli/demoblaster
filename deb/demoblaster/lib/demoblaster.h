
#ifndef DEMOBLASTER_H
#define DEMOBLASTER_H DEMOBLASTER_H

#include "audio/tremor_in_ram.h"
#include "audio/alsaplay.h"
#include "effects/effects.h"
#include "opc/opcgd.h"
#include "colors/palettes.h"
#include "misc/font4x8.h"
#include "misc/misc.h"
#include "gd_db/gd_db.h"

#define clean_free(d) { memset( d, 0, sizeof(*d) ); free( d ); };

#endif

