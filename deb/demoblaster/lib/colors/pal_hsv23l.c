#include "palettes.h"

static uint32_t const palette_hsv23l_data[] = {
   4,
   0x00ffff,
   0xffff00,
   0xff00ff,
   0x00ffff,
};

uint32_t const *palette_hsv23l = palette_hsv23l_data;
