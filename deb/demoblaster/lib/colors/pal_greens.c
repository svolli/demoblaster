#include "palettes.h"

static uint32_t const palette_greens_data[] = {
   8,
   0xF7FCF5,
   0xE5F5E0,
   0xC7E9C0,
   0xA1D99B,

   0x74C476,
   0x41AB5D,
   0x238B45,
   0x005A32,
};
uint32_t const *palette_greens = palette_greens_data;
