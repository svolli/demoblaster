#include "palettes.h"

static uint32_t const palette_prgn_data[] = {
   8,
   0x762A83,
   0x9970AB,
   0xC2A5CF,
   0xE7D4E8,

   0xD9F0D3,
   0xA6DBA0,
   0x5AAE61,
   0x1B7837,
};
uint32_t const *palette_prgn = palette_prgn_data;
