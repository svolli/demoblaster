#include "palettes.h"

static uint32_t const palette_set2_data[] = {
   8,
   0x66C2A5,
   0xFC8D62,
   0x8DA0CB,
   0xE78AC3,

   0xA6D854,
   0xFFD92F,
   0xE5C494,
   0xB3B3B3,
};
uint32_t const *palette_set2 = palette_set2_data;
