#include "palettes.h"

static uint32_t const palette_pastel1_data[] = {
   8,
   0xFBB4AE,
   0xB3CDE3,
   0xCCEBC5,
   0xDECBE4,

   0xFED9A6,
   0xFFFFCC,
   0xE5D8BD,
   0xFDDAEC,
};
uint32_t const *palette_pastel1 = palette_pastel1_data;
