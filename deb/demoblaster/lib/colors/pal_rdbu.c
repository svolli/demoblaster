#include "palettes.h"

static uint32_t const palette_rdbu_data[] = {
   8,
   0xB2182B,
   0xD6604D,
   0xF4A582,
   0xFDDBC7,

   0xD1E5F0,
   0x92C5DE,
   0x4393C3,
   0x2166AC,
};
uint32_t const *palette_rdbu = palette_rdbu_data;
