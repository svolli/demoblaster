#include "palettes.h"

static uint32_t const palette_ylrd_data[] = {
   4,
   0xffee00,
   0xff7000,
   0xee0000,
   0x7f0000,
};
uint32_t const *palette_ylrd = palette_ylrd_data;
