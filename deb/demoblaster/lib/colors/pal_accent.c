#include "palettes.h"

static uint32_t const palette_accent_data[] = {
   8,
   0x7FC97F,
   0xBEAED4,
   0xFDC086,
   0xFFFF99,

   0x386CB0,
   0xF0027F,
   0xBF5B17,
   0x666666,
};
uint32_t const *palette_accent = palette_accent_data;
