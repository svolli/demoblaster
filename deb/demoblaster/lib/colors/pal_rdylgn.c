#include "palettes.h"

static uint32_t const palette_rdylgn_data[] = {
   8,
   0xD73027,
   0xF46D43,
   0xFDAE61,
   0xFEE08B,

   0xD9EF8B,
   0xA6D96A,
   0x66BD63,
   0x1A9850,
};
uint32_t const *palette_rdylgn = palette_rdylgn_data;
