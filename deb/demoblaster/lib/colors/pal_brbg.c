#include "palettes.h"

static uint32_t const palette_brbg_data[] = {
   8,
   0x8C510A,
   0xBF812D,
   0xDFC27D,
   0xF6E8C3,

   0xC7EAE5,
   0x80CDC1,
   0x35978F,
   0x01665E,
};
uint32_t const *palette_brbg = palette_brbg_data;
