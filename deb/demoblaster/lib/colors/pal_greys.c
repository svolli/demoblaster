#include "palettes.h"

static uint32_t const palette_greys_data[] = {
   8,
   0xFFFFFF,
   0xF0F0F0,
   0xD9D9D9,
   0xBDBDBD,

   0x969696,
   0x737373,
   0x525252,
   0x252525,
};
uint32_t const *palette_greys = palette_greys_data;
