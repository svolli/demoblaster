#include "palettes.h"

static uint32_t const palette_whylrd_data[] = {
   5,
   0xffffff,
   0xffee00,
   0xff7000,
   0xee0000,

   0x7f0000,
};
uint32_t const *palette_whylrd = palette_whylrd_data;
