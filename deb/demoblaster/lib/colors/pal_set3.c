#include "palettes.h"

static uint32_t const palette_set3_data[] = {
   8,
   0x8DD3C7,
   0xFFFFB3,
   0xBEBADA,
   0xFB8072,

   0x80B1D3,
   0xFDB462,
   0xB3DE69,
   0xFCCDE5,
};
uint32_t const *palette_set3 = palette_set3_data;
