#include "palettes.h"

static uint32_t const palette_purples_data[] = {
   8,
   0xFCFBFD,
   0xEFEDF5,
   0xDADAEB,
   0xBCBDDC,

   0x9E9AC8,
   0x807DBA,
   0x6A51A3,
   0x4A1486,
};
uint32_t const *palette_purples = palette_purples_data;
