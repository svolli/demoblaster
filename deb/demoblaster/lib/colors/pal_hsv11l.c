#include "palettes.h"

static uint32_t const palette_hsv11l_data[] = {
   4,
   0x0000ff,
   0x00ff00,
   0xff0000,
   0x0000ff,
};

uint32_t const *palette_hsv11l = palette_hsv11l_data;
