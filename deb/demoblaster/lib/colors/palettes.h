#include <stdint.h>

#ifndef OPC_PALETTES_H
#define OPC_PALETTES_H OPC_PALETTES_H

#ifdef __cplusplus
extern "C"
{
#endif

extern uint32_t const *palette_accent;
extern uint32_t const *palette_blues;
extern uint32_t const *palette_brbg;
extern uint32_t const *palette_bugn;
extern uint32_t const *palette_bupu;
extern uint32_t const *palette_dark2;
extern uint32_t const *palette_gnbu;
extern uint32_t const *palette_gnpu;
extern uint32_t const *palette_greens;
extern uint32_t const *palette_greys;
extern uint32_t const *palette_hsv11r;
extern uint32_t const *palette_hsv12r;
extern uint32_t const *palette_hsv13r;
extern uint32_t const *palette_hsv11l;
extern uint32_t const *palette_hsv12l;
extern uint32_t const *palette_hsv13l;
extern uint32_t const *palette_hsv21r;
extern uint32_t const *palette_hsv22r;
extern uint32_t const *palette_hsv23r;
extern uint32_t const *palette_hsv21l;
extern uint32_t const *palette_hsv22l;
extern uint32_t const *palette_hsv23l;
extern uint32_t const *palette_jet;
extern uint32_t const *palette_oranges;
extern uint32_t const *palette_orrd;
extern uint32_t const *palette_paired;
extern uint32_t const *palette_parula;
extern uint32_t const *palette_pastel1;
extern uint32_t const *palette_pastel2;
extern uint32_t const *palette_piyg;
extern uint32_t const *palette_prgn;
extern uint32_t const *palette_pubugn;
extern uint32_t const *palette_pubu;
extern uint32_t const *palette_puor;
extern uint32_t const *palette_purd;
extern uint32_t const *palette_purples;
extern uint32_t const *palette_rdbu;
extern uint32_t const *palette_rdgy;
extern uint32_t const *palette_rdpu;
extern uint32_t const *palette_rdylbu;
extern uint32_t const *palette_rdylgn;
extern uint32_t const *palette_reds;
extern uint32_t const *palette_set1;
extern uint32_t const *palette_set2;
extern uint32_t const *palette_set3;
extern uint32_t const *palette_spectral;
extern uint32_t const *palette_whylrd;
extern uint32_t const *palette_ylgnbu;
extern uint32_t const *palette_ylgn;
extern uint32_t const *palette_ylorbr;
extern uint32_t const *palette_ylorrd;
extern uint32_t const *palette_ylrd;

extern uint32_t const *palette_vcsntsc;

const char *pal2name( uint32_t const *pointer );

#ifdef __cplusplus
}
#endif

#endif
