#include "palettes.h"

static uint32_t const palette_rdylbu_data[] = {
   8,
   0xD73027,
   0xF46D43,
   0xFDAE61,
   0xFEE090,

   0xE0F3F8,
   0xABD9E9,
   0x74ADD1,
   0x4575B4,
};
uint32_t const *palette_rdylbu = palette_rdylbu_data;
