#include "palettes.h"

static uint32_t const palette_hsv11r_data[] = {
   4,
   0xff0000,
   0x00ff00,
   0x0000ff,
   0xff0000,
};

uint32_t const *palette_hsv11r = palette_hsv11r_data;
