#include "palettes.h"

static uint32_t const palette_hsv13l_data[] = {
   4,
   0xff0000,
   0x0000ff,
   0x00ff00,
   0xff0000,
};

uint32_t const *palette_hsv13l = palette_hsv13l_data;
