#include "palettes.h"

static uint32_t const palette_piyg_data[] = {
   8,
   0xC51B7D,
   0xDE77AE,
   0xF1B6DA,
   0xFDE0EF,

   0xE6F5D0,
   0xB8E186,
   0x7FBC41,
   0x4D9221,
};
uint32_t const *palette_piyg = palette_piyg_data;
