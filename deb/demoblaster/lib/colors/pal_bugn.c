#include "palettes.h"

static uint32_t const palette_bugn_data[] = {
   8,
   0xF7FCFD,
   0xE5F5F9,
   0xCCECE6,
   0x99D8C9,

   0x66C2A4,
   0x41AE76,
   0x238B45,
   0x005824,
};
uint32_t const *palette_bugn = palette_bugn_data;
