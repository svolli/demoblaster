#include "palettes.h"

static uint32_t const palette_oranges_data[] = {
   8,
   0xFFF5EB,
   0xFEE6CE,
   0xFDD0A2,
   0xFDAE6B,

   0xFD8D3C,
   0xF16913,
   0xD94801,
   0x8C2D04,
};
uint32_t const *palette_oranges = palette_oranges_data;
