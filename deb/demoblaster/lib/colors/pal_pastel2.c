#include "palettes.h"

static uint32_t const palette_pastel2_data[] = {
   8,
   0xB3E2CD,
   0xFDCDAC,
   0xCDB5E8,
   0xF4CAE4,

   0xD6F5C9,
   0xFFF2AE,
   0xF1E2CC,
   0xCCCCCC,
};
uint32_t const *palette_pastel2 = palette_pastel2_data;
