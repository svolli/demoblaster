#include "palettes.h"

static uint32_t const palette_paired_data[] = {
   8,
   0xA6CEE3,
   0x1F78B4,
   0xB2DF8A,
   0x33A02C,

   0xFB9A99,
   0xE31A1C,
   0xFDBF6F,
   0xFF7F00,
};
uint32_t const *palette_paired = palette_paired_data;
