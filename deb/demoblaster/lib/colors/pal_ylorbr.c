#include "palettes.h"

static uint32_t const palette_ylorbr_data[] = {
   8,
   0xFFFFE5,
   0xFFF7BC,
   0xFEE391,
   0xFEC44F,

   0xFE9929,
   0xEC7014,
   0xCC4C02,
   0x8C2D04,
};
uint32_t const *palette_ylorbr = palette_ylorbr_data;
