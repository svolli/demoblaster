#include "palettes.h"

static uint32_t const palette_blues_data[] = {
   8,
   0xF7FBFF,
   0xDEEBF7,
   0xC6DBEF,
   0x9ECAE1,

   0x6BAED6,
   0x4292C6,
   0x2171B5,
   0x084594,
};
uint32_t const *palette_blues = palette_blues_data;
