#include "palettes.h"

static uint32_t const palette_puor_data[] = {
   8,
   0xB35806,
   0xE08214,
   0xFDB863,
   0xFEE0B6,

   0xD8DAEB,
   0xB2ABD2,
   0x8073AC,
   0x542788,
};
uint32_t const *palette_puor = palette_puor_data;
