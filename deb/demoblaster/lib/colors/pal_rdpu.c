#include "palettes.h"

static uint32_t const palette_rdpu_data[] = {
   8,
   0xFFF7F3,
   0xFDE0DD,
   0xFCC5C0,
   0xFA9FB5,

   0xF768A1,
   0xDD3497,
   0xAE017E,
   0x7A0177,
};
uint32_t const *palette_rdpu = palette_rdpu_data;
