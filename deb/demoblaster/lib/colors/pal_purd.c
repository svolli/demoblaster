#include "palettes.h"

static uint32_t const palette_purd_data[] = {
   8,
   0xF7F4F9,
   0xE7E1EF,
   0xD4B9DA,
   0xC994C7,

   0xDF65B0,
   0xE7298A,
   0xCE1256,
   0x91003F,
};
uint32_t const *palette_purd = palette_purd_data;
