#include "palettes.h"

static uint32_t const palette_dark2_data[] = {
   8,
   0x1B9E77,
   0xD95F02,
   0x7570B3,
   0xE7298A,

   0x66A61E,
   0xE6AB02,
   0xA6761D,
   0x666666,
};
uint32_t const *palette_dark2 = palette_dark2_data;
