#include "palettes.h"

static uint32_t const palette_hsv21l_data[] = {
   4,
   0xffff00,
   0xff00ff,
   0x00ffff,
   0xffff00,
};

uint32_t const *palette_hsv21l = palette_hsv21l_data;
