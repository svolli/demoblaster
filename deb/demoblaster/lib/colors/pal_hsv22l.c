#include "palettes.h"

static uint32_t const palette_hsv22l_data[] = {
   4,
   0xff00ff,
   0x00ffff,
   0xffff00,
   0xff00ff,
};

uint32_t const *palette_hsv22l = palette_hsv22l_data;
