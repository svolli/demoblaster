#include "palettes.h"

static uint32_t const palette_ylgnbu_data[] = {
   8,
   0xFFFFD9,
   0xEDF8B1,
   0xC7E9B4,
   0x7FCDBB,

   0x41B6C4,
   0x1D91C0,
   0x225EA8,
   0x0C2C84,
};
uint32_t const *palette_ylgnbu = palette_ylgnbu_data;
