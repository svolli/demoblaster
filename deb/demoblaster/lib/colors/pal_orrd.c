#include "palettes.h"

static uint32_t const palette_orrd_data[] = {
   8,
   0xFFF7EC,
   0xFEE8C8,
   0xFDD49E,
   0xFDBB84,

   0xFC8D59,
   0xEF6548,
   0xD7301F,
   0x990000,
};
uint32_t const *palette_orrd = palette_orrd_data;
