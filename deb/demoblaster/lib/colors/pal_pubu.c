#include "palettes.h"

static uint32_t const palette_pubu_data[] = {
   8,
   0xFFF7FB,
   0xECE7F2,
   0xD0D1E6,
   0xA6BDDB,

   0x74A9CF,
   0x3690C0,
   0x0570B0,
   0x034E7B,
};
uint32_t const *palette_pubu = palette_pubu_data;
