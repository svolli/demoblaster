#include "palettes.h"

static uint32_t const palette_ylorrd_data[] = {
   8,
   0xFFFFCC,
   0xFFEDA0,
   0xFED976,
   0xFEB24C,

   0xFD8D3C,
   0xFC4E2A,
   0xE31A1C,
   0xB10026,
};
uint32_t const *palette_ylorrd = palette_ylorrd_data;
