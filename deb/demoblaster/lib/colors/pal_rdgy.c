#include "palettes.h"

static uint32_t const palette_rdgy_data[] = {
   8,
   0xB2182B,
   0xD6604D,
   0xF4A582,
   0xFDDBC7,

   0xE0E0E0,
   0xBABABA,
   0x878787,
   0x4D4D4D,
};
uint32_t const *palette_rdgy = palette_rdgy_data;
