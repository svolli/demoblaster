#include "palettes.h"

static uint32_t const palette_ylgn_data[] = {
   8,
   0xFFFFE5,
   0xF7FCB9,
   0xD9F0A3,
   0xADDD8E,

   0x78C679,
   0x41AB5D,
   0x238443,
   0x005A32,
};
uint32_t const *palette_ylgn = palette_ylgn_data;
