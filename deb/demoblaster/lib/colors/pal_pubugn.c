#include "palettes.h"

static uint32_t const palette_pubugn_data[] = {
   8,
   0xFFF7FB,
   0xECE7F0,
   0xD0D1E6,
   0xA6BDDB,

   0x67A9CF,
   0x3690C0,
   0x02818A,
   0x016540,
};
uint32_t const *palette_pubugn = palette_pubugn_data;
