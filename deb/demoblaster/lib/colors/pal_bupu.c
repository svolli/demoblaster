#include "palettes.h"

static uint32_t const palette_bupu_data[] = {
   8,
   0xF7FCFD,
   0xE0ECF4,
   0xBFD3E6,
   0x9EBCDA,

   0x8C96C6,
   0x8C6BB1,
   0x88419D,
   0x6E016B,
};
uint32_t const *palette_bupu = palette_bupu_data;
