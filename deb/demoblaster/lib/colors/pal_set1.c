#include "palettes.h"

static uint32_t const palette_set1_data[] = {
   8,
   0xE41A1C,
   0x377EB8,
   0x4DAF4A,
   0x984EA3,

   0xFF7F00,
   0xFFFF33,
   0xA65628,
   0xF781BF,
};
uint32_t const *palette_set1 = palette_set1_data;
