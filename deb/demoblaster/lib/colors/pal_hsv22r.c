#include "palettes.h"

static uint32_t const palette_hsv22r_data[] = {
   4,
   0xff00ff,
   0xffff00,
   0x00ffff,
   0xff00ff,
};

uint32_t const *palette_hsv22r = palette_hsv22r_data;
