#include "palettes.h"

static uint32_t const palette_hsv23r_data[] = {
   4,
   0xffff00,
   0x00ffff,
   0xff00ff,
   0xffff00,
};

uint32_t const *palette_hsv23r = palette_hsv23r_data;
