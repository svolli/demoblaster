#include "palettes.h"

static uint32_t const palette_parula_data[] = {
   9,
   0x352a87,
   0x0363e1,
   0x1485d4,
   0x06a7c6,

   0x38b99e,
   0x92bf73,
   0xd9ba56,
   0xfcce2e,
   0xf9fb0e,
};
uint32_t const *palette_parula = palette_parula_data;
