#include "palettes.h"

static uint32_t const palette_reds_data[] = {
   8,
   0xFFF5F0,
   0xFEE0D2,
   0xFCBBA1,
   0xFC9272,

   0xFB6A4A,
   0xEF3B2C,
   0xCB181D,
   0x99000D,
};
uint32_t const *palette_reds = palette_reds_data;
