#include "palettes.h"

static uint32_t const palette_hsv21r_data[] = {
   4,
   0x00ffff,
   0xff00ff,
   0xffff00,
   0x00ffff,
};

uint32_t const *palette_hsv21r = palette_hsv21r_data;
