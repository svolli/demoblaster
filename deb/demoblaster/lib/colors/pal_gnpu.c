#include "palettes.h"

static uint32_t const palette_gnpu_data[] = {
   10,
   0x396353,
   0x0db14b,
   0x6dc067,
   0xabd69b,

   0xdaeac1,
   0xdfcce4,
   0xc7b2d6,
   0x9474b4,

   0x754098,
   0x504971,
};
uint32_t const *palette_gnpu = palette_gnpu_data;
