#include "palettes.h"

static uint32_t const palette_gnbu_data[] = {
   8,
   0xF7FCF0,
   0xE0F3DB,
   0xCCEBC5,
   0xA8DDB5,

   0x7BCCC4,
   0x4EB3D3,
   0x2B8CBE,
   0x08589E,
};
uint32_t const *palette_gnbu = palette_gnbu_data;
