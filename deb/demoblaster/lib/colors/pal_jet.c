#include "palettes.h"

static uint32_t const palette_jet_data[] = {
   9,
   0x000080,
   0x0000ff,
   0x0080ff,
   0x00ffff,
   
   0x80ff80,
   0xffff00,
   0xff8000,
   0xff0000,
   
   0x800000,
};
uint32_t const *palette_jet = palette_jet_data;
