#include "palettes.h"

static uint32_t const palette_spectral_data[] = {
   8,
   0xD53E4F,
   0xF46D43,
   0xFDAE61,
   0xFEE08B,

   0xE6F598,
   0xABDDA4,
   0x66C2A5,
   0x3288BD,
};
uint32_t const *palette_spectral = palette_spectral_data;
