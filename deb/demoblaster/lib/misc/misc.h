
#ifndef MISC_MISC_H
#define MISC_MISC_H MISC_MISC_H

extern const unsigned char font4x8[768];

unsigned int isqrt( unsigned int x );
char *ipv4_addresses_string();
char *ipv6_addresses_string();

#endif

