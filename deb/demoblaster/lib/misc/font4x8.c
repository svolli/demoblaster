
#include "font4x8.h"

const unsigned char font4x8[768] = {
/*  : 0x20 */
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
/* !: 0x21 */
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0000,
   0b0100,
   0b0000,
/* ": 0x22 */
   0b1010,
   0b1010,
   0b1010,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
/* #: 0x23 */
   0b0000,
   0b1010,
   0b1110,
   0b1010,
   0b1110,
   0b1010,
   0b0000,
   0b0000,
/* $: 0x24 */
   0b0100,
   0b1110,
   0b1000,
   0b0100,
   0b0010,
   0b1110,
   0b0100,
   0b0000,
/* %: 0x25 */
   0b1010,
   0b0010,
   0b0100,
   0b0100,
   0b0100,
   0b1000,
   0b1010,
   0b0000,
/* &: 0x26 */
   0b0100,
   0b1010,
   0b1010,
   0b0100,
   0b0101,
   0b1010,
   0b1110,
   0b0011,
/* ': 0x27 */
   0b0110,
   0b0010,
   0b0100,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
/* (: 0x28 */
   0b0010,
   0b0100,
   0b1000,
   0b1000,
   0b1000,
   0b0100,
   0b0010,
   0b0000,
/* ): 0x29 */
   0b1000,
   0b0100,
   0b0010,
   0b0010,
   0b0010,
   0b0100,
   0b1000,
   0b0000,
/* *: 0x2a */
   0b0000,
   0b1010,
   0b0100,
   0b1110,
   0b0100,
   0b1010,
   0b0000,
   0b0000,
/* +: 0x2b */
   0b0000,
   0b0100,
   0b0100,
   0b1110,
   0b0100,
   0b0100,
   0b0000,
   0b0000,
/* ,: 0x2c */
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0100,
   0b0100,
   0b1000,
/* -: 0x2d */
   0b0000,
   0b0000,
   0b0000,
   0b1110,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
/* .: 0x2e */
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b1000,
   0b0000,
/* /: 0x2f */
   0b0010,
   0b0010,
   0b0100,
   0b0100,
   0b0100,
   0b1000,
   0b1000,
   0b0000,
/* 0: 0x30 */
   0b1110,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1110,
   0b0000,
/* 1: 0x31 */
   0b0100,
   0b1100,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b1110,
   0b0000,
/* 2: 0x32 */
   0b1100,
   0b0010,
   0b0010,
   0b0100,
   0b0100,
   0b1000,
   0b1110,
   0b0000,
/* 3: 0x33 */
   0b1110,
   0b0010,
   0b0010,
   0b0110,
   0b0010,
   0b0010,
   0b1110,
   0b0000,
/* 4: 0x34 */
   0b1010,
   0b1010,
   0b1010,
   0b1110,
   0b0010,
   0b0010,
   0b0010,
   0b0000,
/* 5: 0x35 */
   0b1110,
   0b1000,
   0b1000,
   0b1110,
   0b0010,
   0b0010,
   0b1100,
   0b0000,
/* 6: 0x36 */
   0b1110,
   0b1000,
   0b1000,
   0b1110,
   0b1010,
   0b1010,
   0b1110,
   0b0000,
/* 7: 0x37 */
   0b1110,
   0b0010,
   0b0010,
   0b0100,
   0b0100,
   0b1000,
   0b1000,
   0b0000,
/* 8: 0x38 */
   0b1110,
   0b1010,
   0b1010,
   0b1110,
   0b1010,
   0b1010,
   0b1110,
   0b0000,
/* 9: 0x39 */
   0b1110,
   0b1010,
   0b1010,
   0b1110,
   0b0010,
   0b0010,
   0b1110,
   0b0000,
/* :: 0x3a */
   0b0000,
   0b0000,
   0b0100,
   0b0000,
   0b0100,
   0b0000,
   0b0000,
   0b0000,
/* ;: 0x3b */
   0b0000,
   0b0000,
   0b0100,
   0b0000,
   0b0000,
   0b0100,
   0b0100,
   0b1000,
/* <: 0x3c */
   0b0000,
   0b0010,
   0b0100,
   0b1000,
   0b0100,
   0b0010,
   0b0000,
   0b0000,
/* =: 0x3d */
   0b0000,
   0b0000,
   0b1110,
   0b0000,
   0b1110,
   0b0000,
   0b0000,
   0b0000,
/* >: 0x3e */
   0b0000,
   0b1000,
   0b0100,
   0b0010,
   0b0100,
   0b1000,
   0b0000,
   0b0000,
/* ?: 0x3f */
   0b0100,
   0b1010,
   0b0010,
   0b0100,
   0b0100,
   0b0000,
   0b0100,
   0b0000,
/* @: 0x40 */
   0b0100,
   0b1010,
   0b1010,
   0b1110,
   0b1110,
   0b1000,
   0b0110,
   0b0000,
/* A: 0x41 */
   0b0100,
   0b1010,
   0b1010,
   0b1110,
   0b1010,
   0b1010,
   0b1010,
   0b0000,
/* B: 0x42 */
   0b1100,
   0b1010,
   0b1010,
   0b1100,
   0b1010,
   0b1010,
   0b1100,
   0b0000,
/* C: 0x43 */
   0b0100,
   0b1010,
   0b1000,
   0b1000,
   0b1000,
   0b1010,
   0b0100,
   0b0000,
/* D: 0x44 */
   0b1100,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1100,
   0b0000,
/* E: 0x45 */
   0b1110,
   0b1000,
   0b1000,
   0b1100,
   0b1000,
   0b1000,
   0b1110,
   0b0000,
/* F: 0x46 */
   0b1110,
   0b1000,
   0b1000,
   0b1100,
   0b1000,
   0b1000,
   0b1000,
   0b0000,
/* G: 0x47 */
   0b0100,
   0b1010,
   0b1000,
   0b1110,
   0b1010,
   0b1010,
   0b0100,
   0b0000,
/* H: 0x48 */
   0b1010,
   0b1010,
   0b1010,
   0b1110,
   0b1010,
   0b1010,
   0b1010,
   0b0000,
/* I: 0x49 */
   0b1110,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b1110,
   0b0000,
/* J: 0x4a */
   0b0010,
   0b0010,
   0b0010,
   0b0010,
   0b0010,
   0b1010,
   0b0100,
   0b0000,
/* K: 0x4b */
   0b1010,
   0b1010,
   0b1100,
   0b1100,
   0b1010,
   0b1010,
   0b1010,
   0b0000,
/* L: 0x4c */
   0b1000,
   0b1000,
   0b1000,
   0b1000,
   0b1000,
   0b1000,
   0b1110,
   0b0000,
/* M: 0x4d */
   0b1010,
   0b1110,
   0b1110,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b0000,
/* N: 0x4e */
   0b1100,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b0000,
/* O: 0x4f */
   0b0100,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b0100,
   0b0000,
/* P: 0x50 */
   0b1100,
   0b1010,
   0b1010,
   0b1100,
   0b1000,
   0b1000,
   0b1000,
   0b0000,
/* Q: 0x51 */
   0b0100,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b0100,
   0b0010,
/* R: 0x52 */
   0b1100,
   0b1010,
   0b1010,
   0b1100,
   0b1010,
   0b1010,
   0b1010,
   0b0000,
/* S: 0x53 */
   0b0100,
   0b1010,
   0b1000,
   0b0100,
   0b0010,
   0b1010,
   0b0100,
   0b0000,
/* T: 0x54 */
   0b1110,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0000,
/* U: 0x55 */
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1110,
   0b0000,
/* V: 0x56 */
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b0100,
   0b0100,
   0b0000,
/* W: 0x57 */
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b1110,
   0b1110,
   0b1010,
   0b0000,
/* X: 0x58 */
   0b1010,
   0b1010,
   0b1110,
   0b0100,
   0b1110,
   0b1010,
   0b1010,
   0b0000,
/* Y: 0x59 */
   0b1010,
   0b1010,
   0b1010,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0000,
/* Z: 0x5a */
   0b1110,
   0b0010,
   0b0100,
   0b0100,
   0b0100,
   0b1000,
   0b1110,
   0b0000,
/* [: 0x5b */
   0b0110,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0110,
   0b0000,
/* \: 0x5c */
   0b1000,
   0b1000,
   0b0100,
   0b0100,
   0b0100,
   0b0010,
   0b0010,
   0b0000,
/* ]: 0x5d */
   0b0110,
   0b0010,
   0b0010,
   0b0010,
   0b0010,
   0b0010,
   0b0110,
   0b0000,
/* ^: 0x5e */
   0b0000,
   0b0100,
   0b1110,
   0b1010,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
/* _: 0x5f */
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b1111,
/* `: 0x60 */
   0b0110,
   0b0100,
   0b0010,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
/* a: 0x61 */
   0b0000,
   0b0000,
   0b1100,
   0b0010,
   0b0110,
   0b1010,
   0b0110,
   0b0000,
/* b: 0x62 */
   0b1000,
   0b1000,
   0b1100,
   0b1010,
   0b1010,
   0b1010,
   0b1100,
   0b0000,
/* c: 0x63 */
   0b0000,
   0b0000,
   0b0100,
   0b1010,
   0b1000,
   0b1010,
   0b0100,
   0b0000,
/* d: 0x64 */
   0b0010,
   0b0010,
   0b0110,
   0b1010,
   0b1010,
   0b1010,
   0b0110,
   0b0000,
/* e: 0x65 */
   0b0000,
   0b0000,
   0b0100,
   0b1010,
   0b1110,
   0b1000,
   0b0110,
   0b0000,
/* f: 0x66 */
   0b0010,
   0b0100,
   0b0100,
   0b1110,
   0b0100,
   0b0100,
   0b0100,
   0b0000,
/* g: 0x67 */
   0b0000,
   0b0000,
   0b0110,
   0b1010,
   0b1010,
   0b0110,
   0b0010,
   0b1100,
/* h: 0x68 */
   0b1000,
   0b1000,
   0b1100,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b0000,
/* i: 0x69 */
   0b0100,
   0b0000,
   0b1100,
   0b0100,
   0b0100,
   0b0100,
   0b1110,
   0b0000,
/* j: 0x6a */
   0b0010,
   0b0000,
   0b0010,
   0b0010,
   0b0010,
   0b0010,
   0b1010,
   0b0100,
/* k: 0x6b */
   0b1000,
   0b1000,
   0b1010,
   0b1010,
   0b1100,
   0b1010,
   0b1010,
   0b0000,
/* l: 0x6c */
   0b1100,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b0100,
   0b1110,
   0b0000,
/* m: 0x6d */
   0b0000,
   0b0000,
   0b1010,
   0b1110,
   0b1010,
   0b1010,
   0b1010,
   0b0000,
/* n: 0x6e */
   0b0000,
   0b0000,
   0b1100,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b0000,
/* o: 0x6f */
   0b0000,
   0b0000,
   0b0100,
   0b1010,
   0b1010,
   0b1010,
   0b0100,
   0b0000,
/* p: 0x70 */
   0b0000,
   0b0000,
   0b1100,
   0b1010,
   0b1010,
   0b1100,
   0b1000,
   0b1000,
/* q: 0x71 */
   0b0000,
   0b0000,
   0b0110,
   0b1010,
   0b1010,
   0b0110,
   0b0010,
   0b0010,
/* r: 0x72 */
   0b0000,
   0b0000,
   0b1100,
   0b1010,
   0b1000,
   0b1000,
   0b1000,
   0b0000,
/* s: 0x73 */
   0b0000,
   0b0000,
   0b0110,
   0b1000,
   0b0100,
   0b0010,
   0b1100,
   0b0000,
/* t: 0x74 */
   0b0000,
   0b0100,
   0b1110,
   0b0100,
   0b0100,
   0b0100,
   0b0010,
   0b0000,
/* u: 0x75 */
   0b0000,
   0b0000,
   0b1010,
   0b1010,
   0b1010,
   0b1010,
   0b0110,
   0b0000,
/* v: 0x76 */
   0b0000,
   0b0000,
   0b1010,
   0b1010,
   0b1010,
   0b0100,
   0b0100,
   0b0000,
/* w: 0x77 */
   0b0000,
   0b0000,
   0b1010,
   0b1010,
   0b1010,
   0b1110,
   0b1010,
   0b0000,
/* x: 0x78 */
   0b0000,
   0b0000,
   0b1010,
   0b1010,
   0b0100,
   0b1010,
   0b1010,
   0b0000,
/* y: 0x79 */
   0b0000,
   0b0000,
   0b1010,
   0b1010,
   0b1010,
   0b0110,
   0b0010,
   0b1100,
/* z: 0x7a */
   0b0000,
   0b0000,
   0b1110,
   0b0010,
   0b0100,
   0b1000,
   0b1110,
   0b0000,
/* {: 0x7b */
   0b0010,
   0b0100,
   0b0100,
   0b1000,
   0b0100,
   0b0100,
   0b0010,
   0b0000,
/* |: 0x7c */
   0b0100,
   0b0100,
   0b0100,
   0b0000,
   0b0100,
   0b0100,
   0b0100,
   0b0000,
/* }: 0x7d */
   0b1000,
   0b0100,
   0b0100,
   0b0010,
   0b0100,
   0b0100,
   0b1000,
   0b0000,
/* ~: 0x7e */
   0b0101,
   0b1010,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
   0b0000,
/* ???: 0x7f */
   0b0000,
   0b0000,
   0b0100,
   0b0100,
   0b1010,
   0b1010,
   0b1110,
   0b0000
};
