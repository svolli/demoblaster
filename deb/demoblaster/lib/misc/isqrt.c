
#include "misc.h"

/*
 * taken from http://www.codecodex.com/wiki/Calculate_an_integer_square_root#C
 */
unsigned int isqrt( unsigned int x )
{
   /* "one" starts at the highest power of four <= than the argument. */
   register unsigned int op = x, res = 0, one = 1 << 30; /* second-to-top bit set */

   while( one > op )
   {
      one >>= 2;
   }
   while( one != 0 )
   {
      if( op >= res + one )
      {
         op -= res + one;
         res += one << 1;  // <-- faster than 2 * one
      }
      res >>= 1;
      one >>= 2;
   }
   return res;
}
