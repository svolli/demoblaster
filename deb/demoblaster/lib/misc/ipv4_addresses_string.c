
/* stripped down version from getifaddrs(3) manpage */

#define _GNU_SOURCE     /* To get defns of NI_MAXSERV and NI_MAXHOST */
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <linux/if_link.h>

#include "misc.h"


char *ipv4_addresses_string()
{
   struct ifaddrs *ifaddr, *ifa;
   int s;
   int len = 0;
   char host[NI_MAXHOST];
   char *retval = 0;

   if( getifaddrs( &ifaddr ) == -1 )
   {
      perror( "getifaddrs" );
      exit( EXIT_FAILURE );
   }

   /* Walk through linked list, maintaining head pointer so we
              can free list later */

   for( ifa = ifaddr; ifa; ifa = ifa->ifa_next )
   {
      /* we only want ipv4 addresses */
      if( ifa->ifa_addr->sa_family == AF_INET )
      {
         s = getnameinfo( ifa->ifa_addr,
                          sizeof(struct sockaddr_in),
                          host, NI_MAXHOST,
                          NULL, 0, NI_NUMERICHOST );
         if( s != 0 )
         {
            printf( "getnameinfo() failed: %s\n", gai_strerror(s) );
            exit( EXIT_FAILURE );
         }

         len += strlen( host ) + 1;
      }
   }

   retval = (char*)malloc( len + 1 );
   retval[0] = '\0';
   for( ifa = ifaddr; ifa; ifa = ifa->ifa_next )
   {
      /* we only want ipv4 addresses */
      if( ifa->ifa_addr->sa_family == AF_INET )
      {
         s = getnameinfo( ifa->ifa_addr,
                          sizeof(struct sockaddr_in),
                          host, NI_MAXHOST,
                          NULL, 0, NI_NUMERICHOST );
         if( s != 0 )
         {
            printf( "getnameinfo() failed: %s\n", gai_strerror(s) );
            exit( EXIT_FAILURE );
         }

         strncat( retval, host, len );
         strncat( retval, " ", len );
      }
   }
   retval[len-1] = '\0';

   freeifaddrs( ifaddr );

   return retval;
}
