
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <opc.h>
#include <assert.h>

#include <misc/misc.h>

#define LINES (8)

#define FLAG_TEST (1<<0)
#define FLAG_IPV6 (1<<1)

/*                                     01234567 */
static char framebuffer_device[]    = "/dev/fb0";
static char *dev = 0;
static const char SENSE_HAT_FB_ID[] = "RPi-Sense FB";
static int direction = 0;
static unsigned char *bits[LINES] = { 0 };
static int strsize = 0;

struct ipaddrs_s
{
   opc_handler    *handler;
   int            width;
   int            pos;
   unsigned char  *bits[LINES];
};

typedef struct ipaddrs_s *ipaddrs_t;


int probe()
{
   /*                                 1         2 2
                            01234567890123456789012 */
   char SYS_CLASS_NAME[] = "/sys/class/graphics/fb0/name";
   char buffer[64];
   FILE *f;
   int i;
   for( i = '0'; i <= '9'; ++i )
   {
      SYS_CLASS_NAME[22] = i;
      f = fopen( SYS_CLASS_NAME, "rb" );
      if( f )
      {
         fread( buffer, 1, sizeof(buffer), f );
         fclose( f );
         if( !strncmp( buffer, SENSE_HAT_FB_ID, sizeof(SENSE_HAT_FB_ID)-1 ) )
         {
            framebuffer_device[7] = i;
            return 0;
         }
      }
   }
   return -1;
}


void handler( u8 channel, u16 count, pixel* pixels )
{
   int i = 0;
   int idx = 0;
   FILE *f = 0;
   unsigned short fbdata[64];

   if( count > 64 )
   {
      count = 64;
   }

   for( i = 0; i < count; ++i )
   {
      if( direction & 1 )
      {
         idx = (7 - (i / 8)) + (8 * (i % 8));
      }
      else
      {
         idx = i;
      }
      if( direction & 2 )
      {
         idx = (count-1)-idx;
      }
      assert( (idx >= 0) && (idx < count) );

      short col =
            ((pixels[i].r & 0xF8) << 8) |
            ((pixels[i].g & 0xFC) << 3) |
            ((pixels[i].b & 0xF8) >> 3);
      fbdata[idx] = col;
   }
   f = fopen( framebuffer_device, "wb" );
   fwrite( fbdata, sizeof(fbdata), 1, f );
   fclose( f );
}


void help( FILE *f, const char *name )
{
   fprintf( f, "%s: opc server for the raspberry pi sense hat\n"
            "options:\n"
            "-t\ttest if hardware is found\n"
            "-6\talso show ipv6 addresses\n"
            "-d dev\tuse framebuffer device \"dev\" (default: autodetect)\n"
            "-n\ttop of display facing north\n"
            "-s\ttop of display facing south\n"
            "-w\ttop of display facing west\n"
            "-e\ttop of display facing east\n"
            "-p port\ttcp port to listen on (default: %d)\n"
            "-h\tthis help\n", name, OPC_DEFAULT_PORT );
}


void interface_addresses_create( unsigned long flags )
{
   int i    = 0;
   int c    = 0;
   int b    = 0;
   int x    = 8;
   int y    = 0;
   char *ipaddrs = ipv4_addresses_string();

   strsize = strlen( ipaddrs );
   if( flags & FLAG_IPV6 )
   {
      char *ipv6addrs = ipv6_addresses_string();
      char *tmpstring = 0;
      strsize += strlen( ipv6addrs ) + 1;
      tmpstring = (char*)calloc( strsize+1, 1 );
      strncpy( tmpstring, ipaddrs,   strsize + 1 );
      strncat( tmpstring, " ",       strsize + 1 );
      strncat( tmpstring, ipv6addrs, strsize + 1 );
      free( ipaddrs );
      free( ipv6addrs );
      ipaddrs = tmpstring;
   }

   for( i = 0; i < LINES; ++i )
   {
      bits[i] = (unsigned char*)calloc( strsize + 4, 4 );
      if( !bits[i] )
      {
         perror( "calloc" );
         exit( 2 );
      }
   }

   for( i = 0; i < strsize; ++i )
   {
      c = ipaddrs[i] - ' ';

      for( b = 0b1000; b; b >>= 1 )
      {
         for( y = 0; y < LINES; ++y )
         {
            bits[y][x] = font4x8[c * LINES + y] & b ? 1 : 0;
         }
         ++x;
      }
   }

   free( ipaddrs );
}


void interface_addresses_done()
{
   int i;
   for( i = 0; i < LINES; ++i )
   {
      if( bits[i] )
      {
         free( bits[i] );
         bits[i] = 0;
      }
   }
}


int interface_addresses_show( int offset )
{
   int x;
   int y;
   pixel fbdata[64] = { 0 };

   for( y = 0; y < LINES; ++y )
   {
      for( x = 0; x < 8; ++x )
      {
         fbdata[y*8+x].r = bits[y][x+offset] ? 0x40 : 0x00;
         fbdata[y*8+x].g = bits[y][x+offset] ? 0x40 : 0x00;
         fbdata[y*8+x].b = bits[y][x+offset] ? 0x40 : 0x00;
      }
   }
   handler( 0, 64, &fbdata[0] );

   return (offset >= (strsize * 4 + 8)) ? 0 : offset + 1;
}


int main(int argc, char** argv)
{
   u16 port = OPC_DEFAULT_PORT;
   int opt;
   opc_source s;
   unsigned long flags = 0;

   while( (opt = getopt(argc, argv, "6d:ehnp:stw")) != -1 )
   {
      switch( opt )
      {
      case '6':
         flags |= FLAG_IPV6;
         break;
      case 't':
         flags |= FLAG_TEST;
         break;
      case 'd':
         dev = optarg;
         break;
      case 'n':
         direction = 0;
         break;
      case 's':
         direction = 2;
         break;
      case 'e':
         direction = 1;
         break;
      case 'w':
         direction = 3;
         break;
      case 'p':
         port = strtol( optarg, 0, 0 );
         if( (port < 1) || (port > 65535) )
         {
            fprintf( stderr, "illegel port: %d\n", port );
            return 1;
         }
         break;
      case 'h':
      default:
         help( opt == 'h' ? stdout : stderr, argv[0] );
         return opt == 'h' ? 0 : 1;
      }
   }

   if( flags & FLAG_TEST )
   {
      return probe() < 0 ? 1 : 0;
   }

   if( !dev )
   {
      if( probe() < 0 )
      {
         fprintf( stderr, "can't find framebuffer device for %s\n",
                  SENSE_HAT_FB_ID );
         return 1;
      }
      else
      {
         dev = framebuffer_device;
      }
   }
   fprintf( stderr, "using framebuffer %s\n", framebuffer_device );
   s = opc_new_source( port );
   if( s < 0 )
   {
      return 1;
   }
   opt = 0;
   for(;;)
   {
      /* start with displaying ip address */
      interface_addresses_create( flags );
      while( !opc_receive( s, handler, 80 ) )
      {
         opt = interface_addresses_show( opt );
         if( !opt )
         {
            interface_addresses_done();
            interface_addresses_create( flags );
         }
      }
      interface_addresses_done();

      /* once the was a connection use a higher timeout before falling back */
      while( opc_receive( s, handler, 30000 ) );
   }
   return 0;
}
