#!/bin/sh

IMAGE_FILE='/etc/fadecandy/boot.png'
AUTOSTART=
AUTOUSER=

cd "$(dirname "${0}")"

if [ ! -f "${IMAGE_FILE}" ]; then
   IMAGE_FILE=""
fi

if [ -x "${AUTOSTART}" ]; then
   IMAGE_FILE=""
   AUTOSTART="$(readlink -f "${AUTOSTART}")"
   AUTOUSER="$(stat -L -c %U "${AUTOSTART}")"
fi

while ! wget -O /dev/null -q http://localhost:7890 ; do
   sleep 0.33
done

/usr/bin/macstart ${IMAGE_FILE}
if [ -n "${AUTOSTART}" -a -n "${AUTOUSER}" ]; then
   exec su "${AUTOUSER}" -c "${AUTOSTART}"
fi

