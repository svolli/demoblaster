
#include <demoblaster.h>
#include <stdio.h>

int main()
{
   gdImagePtr   im;
   mandelbrot_t mandel;
   FILE *f;
   
   im = gdImageCreateTrueColor( 1280, 720 );
   //im = gdImageCreateTrueColor( 32, 16 );
   mandel = mandelbrot_init( im, 1, 512 );
   //mandelbrot_setparams( mandel, 0.5, -0.5, 0.0 );
   mandelbrot_setparams( mandel, 32768.0, -1.49933, 0.0 );
   mandelbrot_setpalette( mandel, palette_spectral );
   mandelbrot_calc( mandel );

   f = fopen( "mandelbrot.png", "w+" );
   gdImagePng( im, f );
   fclose( f );

   return 0;
}
