
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include <demoblaster.h>

#define STEPS (512)
#define PALETTE (48)

#define DEMOBLASTER (1)

int main( int argc, char *argv[] )
{
   int i;
   opc_sink s;
   gdImagePtr im;
#if DEMOBLASTER
#else
   opc_client_description_t ocd;
#endif
   mandelbrot_t mand;

   mandelbrot_float xs = -0.8;
   mandelbrot_float ys = 0.1;
   mandelbrot_float zs = 1.0;
   mandelbrot_float xe = -1.03;
   mandelbrot_float ye = 0.375;
   //mandelbrot_float ze = 1024.0;
   mandelbrot_float ze = 16.0;
  
#if DEMOBLASTER
   s = opc_new_sink( 0 );
   im = gdImageCreateTrueColor( 32, 16 );
#else
   ocd = ocd_from_default();
   s = opc_new_gd_sink( ocd );
   im = opc_create_gd_image( s );
#endif
   mand = mandelbrot_init( im, 3, PALETTE );
   mandelbrot_setpalette( mand, palette_spectral );
   mandelbrot_setpalette( mand, palette_jet );
   mandelbrot_setpalette( mand, palette_accent );
#if 1
   mandelbrot_setparams( mand, zs, xs, ys );
   mandelbrot_calc( mand );
#if DEMOBLASTER
   opc_put_gd_image_32x16z( s, im );
   opc_put_gd_image_32x16z( s, im );
#else
   opc_put_gd_image( s, im );
   opc_put_gd_image( s, im );
#endif
#endif
   

for(;;){
   sleep( 1 );
   for( i = 0; i < STEPS; ++i )
   {
      mandelbrot_float x = xs + ((xe - xs) * i / STEPS);
      mandelbrot_float y = ys + ((ye - ys) * i / STEPS);
      mandelbrot_float z = zs + ((ze - zs) * i / STEPS);
      mandelbrot_setparams( mand, z, x, y );
#if 0
      printf( "step: %d, %d, %d, %f, %f, %f\n", i,
              mandelbrot_minpalette(mand), mandelbrot_maxpalette(mand),
              z, x, y );
      fflush( stdout );
#endif
      mandelbrot_calc( mand );
#if DEMOBLASTER
      opc_put_gd_image_32x16z( s, im );
#else
      opc_put_gd_image( s, im );
#endif
   }

   for( i = STEPS-1; i > 0; --i )
   {
      mandelbrot_float x = xs + ((xe - xs) * i / STEPS);
      mandelbrot_float y = ys + ((ye - ys) * i / STEPS);
      mandelbrot_float z = zs + ((ze - zs) * i / STEPS);
      mandelbrot_setparams( mand, z, x, y );
#if 0
      printf( "step: %d, %f, %f, %f\n", i, z, x, y );
      fflush( stdout );
#endif
      mandelbrot_calc( mand );
#if DEMOBLASTER
      opc_put_gd_image_32x16z( s, im );
#else
      opc_put_gd_image( s, im );
#endif
   }
}

   gdImageDestroy( im );

   return 0;
}
