3rd Party Code
==============

This folder contains code which was written by 3rd parties, but are
required for operation. Including the full project, like with fadecandy,
would have been overkill. So the required files have been copied to
subfolders sorted by origin.

opc
---
These files were take from `https://github.com/zestyping/openpixelcontrol`
out of the `src` directory. They are required to build an OPC client (a
program that connects to a display to display data). Also included is a
server that can be used for simulating a LED display.

cJSON
-----
These files were take from `https://github.com/kbranigan/cJSON`. This seems
to be an updated version of the files `cJSON.c` and `cJSON.h` which are
available in the `opc` repository above. These versions implement more
features and create less warnings during compilation. Also take note that
there the repository is deprecated by `https://github.com/DaveGamble/cJSON`
(nevertheless this version has been used for the past couple of years
without problems so it has been kept this way).

