#!/bin/sh

tar --exclude=fadecandy/debian -acf fadecandy_1.04.orig.tar.xz fadecandy/*
cd fadecandy; dpkg-buildpackage -S -d; cd ..
cd demoblaster; dpkg-buildpackage -S -d; cd ..
